#include"VirtualMemory.h"

using namespace GardenScript;

Memory::VirtualMemory::VirtualMemory()
{
	memset(m_registers,(int)ULLONG_MAX,sizeof(m_registers));
}

Memory::VirtualMemory::~VirtualMemory()
{
}

Memory::VirtualMemory* Memory::VirtualMemory::getInstance()
{
	static VirtualMemory* staticReturn=new VirtualMemory();

	return staticReturn;
}