#include"Lexical.h"
#include"Exceptions.h"

using namespace GardenScript;

#define _OPERATOR(name,value) m_operators.insert(std::pair<std::string,long>(name,(OPERATOR|value)));

LexicalParser::LexicalParser::LexicalParser()
{
	m_currentLine=0;

	_OPERATOR("+",RIGHT_VALUE);
	_OPERATOR("-",RIGHT_VALUE);
	_OPERATOR("*",RIGHT_VALUE);
	_OPERATOR("/",RIGHT_VALUE);
	_OPERATOR("=",RIGHT_VALUE|LEFT_VALUE);
	_OPERATOR("&",RIGHT_VALUE|LEFT_VALUE|DOUBLE);
	_OPERATOR("*",RIGHT_VALUE|LEFT_VALUE|DOUBLE);
	_OPERATOR("^",RIGHT_VALUE);
	_OPERATOR("%",RIGHT_VALUE);
	_OPERATOR("|",RIGHT_VALUE);
	_OPERATOR("~",RIGHT_VALUE);
	_OPERATOR("!",RIGHT_VALUE);
	_OPERATOR("+=",LEFT_VALUE);
	_OPERATOR("-=",LEFT_VALUE);
	_OPERATOR("*=",LEFT_VALUE);
	_OPERATOR("/=",LEFT_VALUE);
	_OPERATOR("==",RIGHT_VALUE);
	_OPERATOR("^=",LEFT_VALUE);
	_OPERATOR("|=",LEFT_VALUE);
	_OPERATOR("&=",LEFT_VALUE);
	_OPERATOR("%=",LEFT_VALUE);
	_OPERATOR("!=",LEFT_VALUE);
	_OPERATOR("<",RIGHT_VALUE);
	_OPERATOR(">",RIGHT_VALUE);
	_OPERATOR("<=",RIGHT_VALUE);
	_OPERATOR(">=",RIGHT_VALUE);
	_OPERATOR("||",RIGHT_VALUE);
	_OPERATOR("&&",RIGHT_VALUE);
	_OPERATOR("::",RIGHT_VALUE);
	_OPERATOR("->",RIGHT_VALUE);
	_OPERATOR(".",RIGHT_VALUE);
	_OPERATOR("++",LEFT_VALUE);
	_OPERATOR("--",LEFT_VALUE);
	
}

LexicalParser::LexicalParser::~LexicalParser()
{
}

LexicalParser::LexicalParser* LexicalParser::LexicalParser::getInstance()
{
	static LexicalParser* staticReturn=new LexicalParser();

	return staticReturn;
}
/*
void LexicalParser::LexicalParser::parseAllLines()
{
	//for(unsigned int i=0;i<Parser::Parser::getInstance()->getLines
}*/

std::vector<std::string> LexicalParser::LexicalParser::stringToTokens(std::string Source)
{
	unsigned int localPosition=0;

	std::vector<std::string> localTokens;
	localTokens.push_back(Text::currentToken(Source,localPosition));

	m_error=false;

	while(localTokens.back()!=";")
	{
		try
		{
			localTokens.push_back(Text::currentToken(Source,localPosition));
		}
		catch(GardenScript::Exceptions::Exception& e)
		{
			Parser::Parser::getInstance()->addError(e);
			m_error=true;
		}
	}

	return localTokens;
}

void LexicalParser::LexicalParser::parseLine(Parser::Line* Line)
{
	//na ko�cu lini ZAWSZE musi by� ;

	std::vector<std::string> localTokens=stringToTokens(Line->m_source);

	if(m_error)
		return;

	std::vector<Lexem> localLexems;
	std::vector<long> localTokensType;

	for(unsigned int i=0;i<localTokens.size();++i)
		localTokensType.push_back(lexemType(localTokens[i]));
}

long LexicalParser::LexicalParser::lexemType(std::string Lexem,long PrevLexem)
{
	
	char localChar=Lexem[0];
	std::vector<std::string> localTokens;

	long localPrevLexem;
	long localResult=0;

	if(isdigit(localChar)||localChar=='\'')
		return RIGHT_VALUE;
	else if(isalpha(localChar))
		return (IDENTIFIER|LEFT_VALUE|RIGHT_VALUE);
	else if(localChar=='\"')
		return (RIGHT_VALUE);
	else if(localChar=='(')
	{
		if(PrevLexem!=-1&&(PrevLexem&IDENTIFIER))
			return OPERATOR|RIGHT_VALUE;
		else
		{
			Lexem[0]=' ';
			Lexem.pop_back();

			localTokens=stringToTokens(Lexem);

			localPrevLexem=-1;

			for(unsigned int i=0;i<localTokens.size();++i)
			{
				localPrevLexem=lexemType(localTokens[i],localPrevLexem);

				if(!(localPrevLexem&IDENTIFIER)&&localResult&IDENTIFIER)
					localResult=localResult^IDENTIFIER;
				
				if(localPrevLexem|RIGHT_VALUE)
					localResult=localResult|RIGHT_VALUE;
			}

			if(localResult&IDENTIFIER)
				localResult=localResult|LEFT_VALUE;

			return localResult;
		}
	}
	else if(localChar==';')
		return 0;

	return m_operators.find(Lexem)->second;
}


Parser::Line LexicalParser::LexicalParser::getCurrentLine()
{
	if(m_currentLine)
		return *m_currentLine;

	return Parser::Line("Undefined",0,0);
}

unsigned int LexicalParser::LexicalParser::getCurrentLineNumber()
{
	if(m_currentLine)
		return m_currentLine->m_lineNumber;

	return 0;
}