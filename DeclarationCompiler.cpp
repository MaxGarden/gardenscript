#include"DeclarationCompiler.h"
#include"ExpressionCompiler.h"
#include"Compiler.h"
#include"Bytecode.h"
#include"Memory.h"
#include<Assembler.h>

using namespace GardenScript;

Declaration::Compiler::Compiler()
{
	m_stackSize=0;
}

Declaration::Compiler::~Compiler()
{
}

Declaration::Compiler* Declaration::Compiler::getInstance()
{
	static Compiler* staticReturn=new Compiler();

	return staticReturn;
}

Declaration::bytecode Declaration::Compiler::compileDeclaration(std::vector<Memory::MemoryObject*> Objects)
{
	if(Objects.empty()||GardenScript::Parser::Parser::getInstance()->isError())
		return "";

	bytecode localResult;

	for(unsigned int i=0;i<Objects.size();++i)
	{
		if(!Objects[i]->m_identyfier)
			localResult+=compileIndentyfier(Objects[i]);
	}

	return localResult;
}

Declaration::bytecode Declaration::Compiler::compileIndentyfier(Memory::MemoryObject* Object)
{
	unsigned char localObjectSize=Object->m_type.getSize();

	if(Object->m_type.m_flags&TypeParser::T_REFERENCE)
	{
		Object->m_identyfier=((Expression::IdentifierOperator*)Object->m_defaultValue)->getObject()->m_identyfier;
		
		return bytecode();
	}

	Object->m_identyfier=new Bytecode::Identifier(localObjectSize);

	m_stackSize+=localObjectSize;

	Object->m_identyfier->m_stackAddress=m_stackSize;

	if(Object->m_type.m_arrayDimension.size())
		return compileArray(Object);
	else if(Object->m_multipleDefine)
		return compileFunction(Object);
	else
		return GardenScript::Expression::Compiler::getInstance()->compileExpression(Object->m_defaultValue);
}

Declaration::bytecode Declaration::Compiler::compileFunction(Memory::MemoryObject* Object)
{
	bytecode localResult;

	Memory::Function* localFunction=(Memory::Function*)Object;

	for(unsigned int i=0;i<localFunction->getVariantsSize();++i)
	{
		GardenScript::Declaration::Compiler::getInstance()->pushScope(localResult);

		localFunction->setScope(i,Memory::MemoryManager::getInstance()->getCurrentScope());

		//throw Exceptions::CompilerException("tutaj skonczyłem");
		localResult+=GardenScript::Parser::Parser::getInstance()->internalParse(localFunction->getBody(i),localResult.size());

		GardenScript::Declaration::Compiler::getInstance()->popScope(localResult);
	}

	return localResult;
}

Declaration::bytecode Declaration::Compiler::compileArray(Memory::MemoryObject* Object)
{
	bytecode localResult;
	unsigned __int64 localSize=GardenScript::Compiler::Compiler::getInstance()->getMachineType();
	unsigned __int64 localNewAddress;

	void* localDefaultData;
	unsigned int localDefaultDataSize;

	Expression::BinaryOperator* localDefaultValue;

	localNewAddress=Memory::MemoryManager::getInstance()->getCurrentScope()->getNextAddress(Object->m_type.getStackSize());
	m_stackSize+=Object->m_type.getStackSize();

	localResult.push_back(Assembler::SRV);
	localResult.push_back(Assembler::RESULT);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(localNewAddress+Object->m_type.getStackSize()));

	localResult.push_back(Assembler::ARI);
	localResult.push_back(GardenScript::Expression::BinaryOperator::s_subtractionOperator);
	localResult.push_back(Assembler::LAST_SCOPE_END);
	localResult.push_back(Assembler::RESULT);

	localResult.push_back(Assembler::SSC);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(Object->m_identyfier->m_stackAddress));
	localResult.push_back(Assembler::RESULT);
	localResult.push_back((char)localSize);
	
	localDefaultValue=dynamic_cast<Expression::BinaryOperator*>(Object->m_defaultValue);
	
	if(localDefaultValue)
	{
		if(localDefaultValue->m_right->m_type==Expression::Expression::STATIC_DATA)
		{
			localDefaultData=((Expression::StaticDataOperator*)localDefaultValue->m_right)->createAdjutedData(Object->m_type.getSizeByDereference(),localDefaultDataSize);
		
			if(localDefaultDataSize>Object->m_type.getStackSize())
				throw Exceptions::DeclarationException("Too many initializers!");

			if(localDefaultDataSize!=Object->m_type.getStackSize())
			{
				localResult.push_back(Assembler::SMZ);
				localResult.push_back(Assembler::RESULT);
				localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(Object->m_type.getStackSize()));
			}

			localResult.push_back(Assembler::CPY);
			localResult.push_back(Assembler::RESULT);
			localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(localDefaultDataSize));
			localResult.append((char*)localDefaultData,(unsigned int)localDefaultDataSize);
		}
		else
			throw Exceptions::DeclarationException("Array initialization needs curly braces!");
	}
	
	return localResult;
}

void Declaration::Compiler::pushScope(bytecode& Bytecode)
{
	Memory::MemoryManager::getInstance()->pushScope(new Memory::Scope);

	m_pushOffsets.push_back(Bytecode.size());

	Bytecode.push_back(Assembler::PSH);
	Bytecode.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(0));
} 

void Declaration::Compiler::popScope(bytecode& Bytecode)
{
	if(!Memory::MemoryManager::getInstance()->getCurrentScope())
		throw Exceptions::DeclarationException("Cannot pop a scope when Memory::MemoryManager::getInstance()->getCurrentScope()==0!");

	if(Bytecode[m_pushOffsets.back()]!=Assembler::PSH)
		throw Exceptions::DeclarationException("Bytecode[m_pushOffsets.back()]!=Assembler::PSH");

	bytecode localStackSizeStr=GardenScript::Compiler::Compiler::getInstance()->adjustConstant(GardenScript::Memory::MemoryManager::getInstance()->getCurrentScope()->getScopeSize());

	++m_pushOffsets.back();

	Bytecode.replace(Bytecode.begin()+m_pushOffsets.back(),Bytecode.begin()+m_pushOffsets.back()+localStackSizeStr.size(),localStackSizeStr.begin(),localStackSizeStr.end());

	m_pushOffsets.pop_back();

	Bytecode.push_back(Assembler::POP);
	Bytecode.append(localStackSizeStr);

	m_stackSize-=GardenScript::Memory::MemoryManager::getInstance()->getCurrentScope()->getScopeSize();

	Memory::MemoryManager::getInstance()->popScope();
}