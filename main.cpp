#include<iostream>
#include"Lexical.h"
#include"Parser.h"
#include"Exceptions.h"
#include"Expression.h"
#include"Constant.h"
#include"Type.h"
#include"Declaration.h"
#include"ExpressionCompiler.h"
#include"Compiler.h"

int main(int argc,char* argv[])
{
	std::string localScript;
	unsigned int localPosition=0;
	
	FILE* localFile;

	if(argc<=1)
		fopen_s(&localFile,"script.gs","r");
	else
		fopen_s(&localFile,argv[1],"r");

	if(!localFile)
		return -1;

	setlocale (LC_NUMERIC, "C");

	for(char i=fgetc(localFile);i!=EOF;i=fgetc(localFile))
		localScript+=i;

	fclose(localFile);
	
	bool localRun=true;

	std::string localBufor;
	//setlocale(LC_ALL,"");

	if(argc<=1)
		std::cout<<"compiling: "<<"script.gs"<<std::endl<<std::endl;
	else
	std::cout<<"compiling: "<<argv[1]<<std::endl<<std::endl;

	//std::cout<<"ZROBI� TAK, �EBY OBIEKTY TWORZY�Y SI� TYLKO PRZY \"PUSH SCOPE\" (WSZYSTKIE ZE SCOPE ZEBRAC DO KUPY I ZAINICJOWAC NA POCZATKU)\n\nDODA� OBS�UG� WYRA�EN W PIERSZYM Z FORA (for(x=0...)\n\n";
	//std::cout<<"PRZEPISA� PARSER I KOMPILATOR IFA TAK ZEBY DZIALA�Y DRZEWKA IF ELSE IF...\n";
	//std::cout<<"PRZEPISAC CALY INSTRUCTION COMPILER BO TO JEDEN WIELKI SKELJONY TASMA SYF!\n\n\n";
	//std::cout<<"Przepisuj� :3\n\n\n";
	//||print *(tab-*(&l1)); naprawic 

	// robi� operatory||
	//wyrzuca� b��d dla int** y=&&x;
	//||adres sta�ych &3 
	//przepisa� dereferencje na dynamiczne identifiers, (script.gs nie dzia�a ze wzgl�du na to, �e DEREFERENCE nei jest przepisane na (DYNAMIC_)IDENTIFIERS|| (zrobione inaczej)
	// || interpretacja liczb ujemnych nie dzia�a i=-1 i>0 ...\n\n\n
	//u g�ry zrobione :3



	//std::cout<<"przerwa� kompilacje po wyst�pieniu b��du (dla sekcji) ||parseFunction || ograniczy� wielko�� expression do ~120 wyraz�w||poprawi� type cast || dostosowywanie rozmiaru sta�ej zale�nie od warto�ci";

	//GardenScript::Parser::Line* localLine=new GardenScript::Parser::Line(localScript,0,0);

	//GardenScript::LexicalParser::LexicalParser::getInstance()->parseLine(localLine);

	//GardenScript::Declaration::Parser::getInstance()->parseDeclaration(localScript,localPosition);

	std::string localBytecode=GardenScript::Compiler::Compiler::getInstance()->compile(localScript,localPosition);
	/*localPosition=0;
	unsigned int localLines=0;

	
	try{
		GardenScript::Compiler::Compiler::getInstance()->setRegisterType(GardenScript::Compiler::Compiler::_32b);
		localBytecode=GardenScript::Expression::Compiler::getInstance()->compile(GardenScript::Expression::Parser::getInstance()->parseExpression(GardenScript::Parser::Text::currentLine(localScript,localPosition,localLines)));
	}
	catch(GardenScript::Exceptions::Exception& e)
	{
		std::cout<<e.getMessage();
	}
	*/

	FILE* localBytecodeF;

	fopen_s(&localBytecodeF,"bytecode.gsc","wb");

	if(!localBytecodeF)
		return -2;

	fwrite(&localBytecode[0],1,localBytecode.size(),localBytecodeF);

	fclose(localBytecodeF);
	
//	localExpression=localExpression;

	/*while(localRun)
	{
		try
		{
			localBufor=GardenScript::LexicalParser::Text::currentToken(localScript,localPosition);

			if(localBufor==";")
				localRun=false;

			std::cout<<localBufor<<std::endl;
		}
		catch(GardenScript::Exceptions::Exception& e)
		{
			std::cout<<e.getMessage()<<std::endl;
		}
	}*/

	//std::cout<<"nie dziala typecast ;_;\n\n";


	std::cout<<"log: "<<std::endl;
	GardenScript::Parser::Parser::getInstance()->showLog();

	std::cout<<std::endl<<std::endl<<"done";

	std::cin.get();

	return 0;
}