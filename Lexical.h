#pragma once
#include"Parser.h"
#include<map>
#include"LexicalText.h"

namespace GardenScript
{
	namespace LexicalParser
	{
		enum TokenType
		{
			IDENTIFIER=0x0000000f,
			OPERATOR=0x000000f0,
			CONSTANT=0x00000f00,
			LEFT_VALUE=0x0000f000,
			RIGHT_VALUE=0x000f0000,
			STRING=0x00f00000,
			DOUBLE=0x0f000000,
			BRACKET=0xf0000000
		};

		/*enum IdentifierType
		{
			LEFT_VALUE=0x0000f000,
			RIGHT_VALUE=0x000f0000
		};*/

		struct Lexem
		{
			TokenType m_leftSide;
			TokenType m_rightSide;

			TokenType m_type;
		};

		class LexicalParser
		{
		private:
			LexicalParser();
			~LexicalParser();
		protected:
			//std::vector
			std::map<std::string,long> m_operators;
			std::map<std::string,bool> m_identifiers; //bool-left value
			Parser::Line* m_currentLine;
			bool m_error;
		private:
			std::vector<std::string> stringToTokens(std::string Source);
			long lexemType(std::string Lexem,long PrevLexem=-1);
			bool isAllowedNameChar(unsigned char Char);
		public:
			static LexicalParser* getInstance();

			void parseLine(Parser::Line* Line);
			//void parseAllLines();

			Parser::Line getCurrentLine();
			unsigned int getCurrentLineNumber();

		};
	}
};