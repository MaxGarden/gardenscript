#pragma once

namespace GardenScript
{
	namespace Bytecode
	{
		/*class object
		{
		};*/

		class Identifier
		{
		public:
			unsigned _int64 m_address;
			unsigned _int64 m_stackAddress;
			unsigned char m_size;
			unsigned int m_scope;
		public:
			Identifier(unsigned char Size,bool ReserveAddress=true);
			Identifier(unsigned int Address,unsigned char Size);
			~Identifier();

			unsigned char m_registerAddress[3];

			unsigned _int64 getAddress();
			unsigned char getSize();
		};

		class Constant
		{
		protected:
			void* m_value;
			unsigned char m_size;
		public:
			Constant(void* Value,unsigned char Size);
			~Constant();

			void* getValue();
			unsigned char getSize();
		};
	}
}