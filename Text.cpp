﻿#include"Text.h"
#include"Expression.h"
#include"Instruction.h"

#include<stdarg.h>

using namespace GardenScript::Parser;

void Text::adjust(std::string Input,unsigned int& Position)
{
	while(Position<Input.size()&&isspace(Input[Position]))
		++Position;
}

unsigned char Text::currentChar(std::string Input,unsigned int& Position,bool Update)
{
	if(Position>=Input.size())
		return '\0';

	unsigned int localPosition=Position;

	adjust(Input,localPosition);

	if(Update)
		Position=++localPosition;

	if(Input[localPosition-Update]=='\\')
	{
		if(Update)
			Position=localPosition+1;

		if(localPosition>=Input.size())
		{
			Parser::getInstance()->addError(GardenScript::Exceptions::TextException("Did you forget next character after \'\\\'?"));
			return ' ';
		}
		else
		{
			switch(Input[localPosition])
			{
			case 'n':
				return '\n';
			case 't':
				return '\t';
			case '\n':
				return Update?(currentChar(Input,Position,Update)):(currentChar(Input,localPosition,Update));
			case '\\':
				return '\\';
			case '\'':
				return '\'';
			case '\"':
				return '\"';
			};

			return Input[localPosition];
		}
	}

	return Input[localPosition-Update];
}

unsigned char Text::_currentChar(std::string Input,unsigned int& Position,bool Update)
{
	if(Position>=Input.size())
		return '\0';

	unsigned int localPosition=Position;

	if(Update)
		Position=++localPosition;

	if(Input[localPosition-Update]=='\\')
	{
		if(Update)
			Position=localPosition+1;

		if(localPosition>=Input.size())
		{
			Parser::getInstance()->addError(GardenScript::Exceptions::TextException("Did you forget next character after \'\\\'?"));
			return ' ';
		}
		else
		{
			switch(Input[localPosition])
			{
			case 'n':
				return '\n';
			case 't':
				return '\t';
			case '\n':
				return Update?(currentChar(Input,Position,Update)):(currentChar(Input,localPosition,Update));
			case '\\':
				return '\\';
			case '\'':
				return '\'';
			case '\"':
				return '\"';
			};

			return Input[localPosition];
		}
	}

	return Input[localPosition-Update];
}

unsigned char Text::__currentChar(std::string Input,unsigned int& Position,bool Update)
{
	if(Position>=Input.size())
		return '\0';

	unsigned int localPosition=Position;

	if(Update)
		Position=++localPosition;

	return Input[localPosition-Update];
}

bool Text::checkChar(char Char,std::string Pattern)
{
	//TODO mo¿na przyspieszyæ tworz¹c tablicê jak przy sortowaniu przez zliczanie
	for(unsigned int i=0;i<Pattern.size();++i)
		if(Pattern[i]==Char)
			return true;

	return false;
}

unsigned int Text::patternIndexChar(char Char,std::string Pattern)
{
	//TODO mo¿na przyspieszyæ tworz¹c tablicê jak przy sortowaniu przez zliczanie
	for(unsigned int i=0;i<Pattern.size();++i)
		if(Pattern[i]==Char)
			return i;

	throw Exceptions::TextException(stringFormat("[parser]Character \"%c\" not in the set \"%s\"!",Char,Pattern.c_str()));
	
	return 0;
}

bool Text::checkString(std::string String,std::vector<std::string> Pattern)
{
	for(unsigned int i=0;i<Pattern.size();++i)
		if(Pattern[i]==String)
			return true;

	return false;
}

std::string Text::stringFormat(std::string String,...)
{
	char localBuf[2048];
	std::string localString;

	va_list localVaList;
	va_start(localVaList,String);

	vsprintf_s(localBuf,String.c_str(),localVaList);

	va_end(localVaList);


	localString=localBuf;

	return localString;
}

bool Text::equalString(std::string Input,unsigned int& Position,std::string Pattern,bool WhiteChars,bool Update)
{
	unsigned int localPosition=Position;
	unsigned int localPatternPosition=0;

	char localInputChar,localPatternChar;

	for(unsigned int i=0;i<Pattern.size();++i)
	{
		if(WhiteChars&&(localPosition>=Input.size()||Input[localPosition++]!=Pattern[i]))
			return false;
		else
		{
			localInputChar=currentChar(Input,localPosition);
			localPatternChar=currentChar(Pattern,localPatternPosition);

			if(localInputChar=='\0'||localPatternChar=='\0'||localInputChar!=localPatternChar)
				return false;
		}	
	}

	if(Update)
		Position=localPosition;
			
	return true;
}
			
std::string Text::cutString(std::string Input,unsigned int Begin,int End)
{
	std::string localResult="";

	if(End<=0||(unsigned int)End>Input.size())
		End=Input.size();
	
	for(unsigned int i=Begin;i<(unsigned int)End;++i)
		localResult+=Input[i];

	return localResult;
}

std::string Text::currentBody(std::string Input,unsigned int& Position,char Incrementation,char Decrementation,unsigned int Counter,bool Update)
{
	unsigned int localPosition=Position;
	std::string localResult;
	unsigned char localChar;
	bool localSlash=false;
	
	do
	{
		localSlash=false;

		if(Input[localPosition]=='\\')
			localSlash=true;

		localChar=_currentChar(Input,localPosition);

		if(localChar=='\0')
			throw Exceptions::TextException(Text::stringFormat("Did u forgot \'%c\'?",Decrementation));

		if(localChar==Incrementation&&!localSlash)
			++Counter;
		
		if(localChar==Decrementation&&!localSlash)
			--Counter;

		localResult+=localChar;

	}while(Counter);


	localResult.pop_back();

	if(Update)
		Position=localPosition;

	return localResult;
}

std::string Text::currentString(std::string Input,unsigned int& Position,bool Update)
{
	std::string localReturn="";
	unsigned int localPosition=Position;

	adjust(Input,localPosition);

	for(;localPosition<Input.size();++localPosition)
	{
		if(isspace(Input[localPosition])||Input[localPosition]=='\0')
			break;

		localReturn+=Input[localPosition];
	}

	if(Update)
		Position=localPosition;

	return localReturn;
}

bool Text::isName(std::string Input)
{
	bool localReturn=true;
	unsigned int localPosition=0;

	adjust(Input,localPosition);

	for(unsigned int i=localPosition;i<Input.size();++i)
	{
		if(i>localPosition&&isdigit(Input[i]))
			continue;
		else if(isalpha(Input[i])||checkChar(Input[i],constAllowedNameChars))
			continue;
		
		localReturn=false;
		break;
	}

	return localReturn;
}

std::string Text::currentLine(std::string Input,unsigned int& Position,unsigned int& Lines,bool Update)
{
	std::string localReturn="";
	unsigned int localPosition=Position;

	unsigned char localChar=__currentChar(Input,localPosition);

	while(localChar!='\0')
	{
		if(localChar==';')
			break;
		
		if(localChar=='{')
		{
			localReturn+='{';

			if(Update)
				Position=localPosition;

			localReturn+=currentBody(Input,localPosition,'{','}',1);
			localReturn+='}';
		}
		else
			localReturn+=localChar;
		
		localChar=__currentChar(Input,localPosition);
	}

	for(unsigned int i=Position+1;i<localPosition;++i)
		if(Input[i]=='\n')
			++Lines;

	if(Update)
		Position=localPosition;

	localPosition=0;
	adjust(localReturn,localPosition);

	if(localPosition==localReturn.size())
		localReturn="";

	while(localReturn.size()&&isspace(localReturn.back()))
		localReturn.pop_back();

	if(localChar=='\0'&&localReturn.size()&&localReturn.back()!='}')
		throw Exceptions::TextException("Did you forgot \";\"?");

	return localReturn;
}

std::string Text::currentLine(std::string Input,unsigned int& Position,bool Update,bool RAW)
{
	std::string localReturn="";
	unsigned int localPosition=Position;

	bool localFirst=true;

	unsigned char localChar=__currentChar(Input,localPosition);

	while(localChar!='\0')
	{
		if(localChar==';')
			break;
		
		if(localChar=='{')
		{
			if(!localFirst||RAW)
				localReturn+='{';

			localReturn+=currentBody(Input,localPosition,'{','}',1);

			if(!localFirst)
				localReturn+='}';
			else
			{
				if(RAW)
					localReturn+='}';

				break;
			}
		}
		else
			localReturn+=localChar;
		
		localChar=__currentChar(Input,localPosition);

		if(!isspace(localChar)&&localChar!='{')
			localFirst=false;
	}

	Parser::getInstance()->addWarning("Użyto currentLine bez Line!","text");

	if(Update)
		Position=localPosition;

	while(localReturn.size()&&isspace(localReturn.back()))
		localReturn.pop_back();

	if(localChar=='\0'&&localReturn.back()!='}')
		throw Exceptions::TextException("Did you forgot \";\"?");

	return localReturn;
}

std::string Text::currentName(std::string Input,unsigned int& Position,bool Update)
{
	std::string localReturn="";
	unsigned int localPosition=Position;

	adjust(Input,localPosition);

	for(;localPosition<Input.size();++localPosition)
	{
		localReturn+=Input[localPosition];

		if(!isName(localReturn))
		{
			localReturn.pop_back();
			break;
		}
	}

	if(Update)
		Position=localPosition;

	return localReturn;
}

std::string Text::currentExpression(std::string Input,unsigned int& Position,unsigned char StopChar,bool Update)
{
	std::string localReturn="";
	unsigned int localPosition=Position;

	adjust(Input,localPosition);

	for(;localPosition<Input.size();++localPosition)
	{
		if(Input[localPosition]=='(')
			localReturn+=stringFormat("(%s)",currentBody(Input,++localPosition,'(',')',1).c_str());
		else if(Input[localPosition]=='{')
			localReturn+=stringFormat("{%s}",currentBody(Input,++localPosition,'{','}',1).c_str());
		else if(Input[localPosition]=='[')
			localReturn+=stringFormat("[%s]",currentBody(Input,++localPosition,'[',']',1).c_str());
		//else if(Input[localPosition]=='<')
		//	localReturn+=stringFormat("<%s>",currentBody(Input,++localPosition,'<','>',1).c_str());

		if(Input[localPosition]==StopChar)
			break;
		else
			localReturn+=Input[localPosition];
	}

	if(Update)
		Position=localPosition;

	return localReturn;
}

std::string Text::currentArgument(std::string Input,unsigned int& Position,std::string EndChars,bool Update)
{
	std::string localResult="";
	unsigned int localPosition=Position;

	char localChar;//=currentChar(Input,localPosition);

	for(;localPosition<Input.size();)
	{
		localChar=_currentChar(Input,localPosition);

		if(checkChar(localChar,constBeginBrackets))
		{
			localResult+=localChar;
			localResult+=currentBody(Input,localPosition,localChar,constEndBrackets[patternIndexChar(localChar,constBeginBrackets)],1);
			localResult+=constEndBrackets[patternIndexChar(localChar,constBeginBrackets)];
			
			continue;
		}
		else if(checkChar(localChar,EndChars))
			break;

		localResult+=localChar;
	}

	if(Update)
		Position=localPosition;

	return localResult;
}

std::string Text::currentOperator(std::string Input,unsigned int& Position,long Flags,char* OperatorID)
{
	std::string localResult;

	std::string localBuffer;
	char localOperatorID;

	adjust(Input,Position);

	while(1)
	{
		localResult+=Input[Position];

		localOperatorID=Expression::Parser::getInstance()->getOperatorID(localResult,Flags);

		if(localOperatorID>=0)
		{
			if(OperatorID)
				*OperatorID=localOperatorID;

			localBuffer=localResult;
		}
		else if(localBuffer.size())
			return localBuffer;
	

		if(isAllowedNameChar(localResult.back())||isdigit(localResult.back())||localResult.back()=='\''||localResult.back()=='\"')
		{
			localResult.pop_back();
			break;
		}
		else if(localResult.back()=='\0')
			break;

		++Position;
	}
	
	return localResult;
}

bool Text::isAllowedNameChar(unsigned char Char)
{
	return (isalpha(Char)||checkChar(Char,constAllowedNameChars));
}

