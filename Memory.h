#pragma once
#include<map>
#include<vector>
#include<string>
#include"Type.h"
#include"Expression.h"
#include"Bytecode.h"

namespace GardenScript
{
	namespace Memory
	{
		struct MemoryObject
		{
			MemoryObject();

			Bytecode::Identifier* m_identyfier;
			TypeParser::ReturnType m_type;
			Expression::Expression* m_defaultValue;
			bool m_multipleDefine;
		};

		class Scope
		{
		protected:
			unsigned __int64 m_currentAddress;
			std::map<std::string,MemoryObject*> m_scope;
		public:
			Scope();
			~Scope();

			void addObject(MemoryObject* Object);
			bool isObject(std::string Name);
			void setObject(std::string Name,MemoryObject* Object);
			MemoryObject* getObject(std::string Name);
			__int64 getScopeSize();

			unsigned __int64 getNextAddress(unsigned __int64 Size,bool Reserve=true);
		};

		class MemoryManager
		{
		private:
			MemoryManager();
			~MemoryManager();
		protected:
			std::vector<Scope*> m_memory;
		public:
			Scope* getCurrentScope();

			unsigned int getCurrentScopeIndex();

			bool isObject(std::string Name);
			MemoryObject* getObject(std::string Name);

			void pushScope(Scope* scope);
			void popScope();

			static MemoryManager* getInstance();
		};
	}
}