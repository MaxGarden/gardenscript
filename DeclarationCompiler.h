#pragma once
#include"Declaration.h"

namespace GardenScript
{
	namespace Declaration
	{
		typedef std::string bytecode;

		class Compiler
		{
		private:
			Compiler();
			~Compiler();
		protected:
			unsigned _int64 m_stackSize;
			std::vector<unsigned int> m_pushOffsets;
		public:
			void pushScope(bytecode& Bytecode);
			void popScope(bytecode& Bytecode);
		private:
			bytecode compileIndentyfier(Memory::MemoryObject* Object);
			bytecode compileFunction(Memory::MemoryObject* Object);
			bytecode compileArray(Memory::MemoryObject* Object);
		public:
			bytecode compileDeclaration(std::vector<Memory::MemoryObject*> Objects);

			static Compiler* getInstance();	
		};
	}
}