#pragma once 
#include<vector>

namespace GardenScript
{
	namespace Memory
	{
		class VirtualMemory
		{
		public:
			static const unsigned int REGISTER_SIZE=256;
		private:
			VirtualMemory();
			~VirtualMemory();
		public:
			__int64 m_registers[257];

			std::vector<__int64> m_stack;

			std::vector<char*> m_memory;
		public:
			static VirtualMemory* getInstance();
		};
	}
}