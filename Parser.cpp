﻿#include"Parser.h"
#include"Text.h"
#include<iostream>
#include"Type.h"
#include"Expression.h"
#include"ExpressionCompiler.h"
#include"Declaration.h"
#include"DeclarationCompiler.h"
#include"Instruction.h"

using namespace GardenScript;

Parser::Line::Line(std::string Source,LexicalParser::Lexem* Lexem,unsigned int LineNumber):m_source(Source),m_lexem(Lexem),m_lineNumber(LineNumber)
{
}

Parser::Line::~Line()
{
	//delete m_lexem;
}

Parser::Parser::Parser()
{
	m_stringOperators.push_back("+");
	m_stringOperators.push_back("-");
	m_stringOperators.push_back("*");
	m_stringOperators.push_back("/");
	m_stringOperators.push_back("=");
	m_stringOperators.push_back("&");
	m_stringOperators.push_back("*");
	m_stringOperators.push_back("^");
	m_stringOperators.push_back("%");
	m_stringOperators.push_back("|");
	m_stringOperators.push_back("~");
	m_stringOperators.push_back("!");
	m_stringOperators.push_back("+=");
	m_stringOperators.push_back("-=");
	m_stringOperators.push_back("*=");
	m_stringOperators.push_back("/=");
	m_stringOperators.push_back("==");
	m_stringOperators.push_back("^=");
	m_stringOperators.push_back("|=");
	m_stringOperators.push_back("&=");
	m_stringOperators.push_back("%=");
	m_stringOperators.push_back("!=");
	m_stringOperators.push_back("<");
	m_stringOperators.push_back(">");
	m_stringOperators.push_back("<=");
	m_stringOperators.push_back(">=");
	m_stringOperators.push_back("||");
	m_stringOperators.push_back("&&");
	m_stringOperators.push_back("::");
	m_stringOperators.push_back("->");
	m_stringOperators.push_back(".");
	m_stringOperators.push_back("++");
	m_stringOperators.push_back("--");

	m_currentLineNumber=m_beginLineNumber=0;
}

Parser::Parser::~Parser()
{
}

Parser::Parser* Parser::Parser::getInstance()
{
	static Parser* staticReturn=new Parser();

	return staticReturn;
}

void Parser::Parser::addError(Exceptions::Exception Error)
{
	m_error=true;
	m_log.push_back(Error.getMessage());

	if(Error.getFatal())
	{
		showLog();
		exit(1);
//		throw Exceptions::FatalError();
	}
}

void Parser::Parser::addWarning(std::string Warning,std::string Type)
{
	m_log.push_back(Text::stringFormat("[%s warning](%s): %s",Type.c_str(),getCurrentLineNumberStr().c_str(),Warning.c_str()));
}

void Parser::Parser::showLog()
{
	for(unsigned int i=0;i<m_log.size();++i)
		std::cout<<m_log[i]<<std::endl;
}

std::string Parser::Parser::_parse(std::string Input,unsigned int& Position)
{
	unsigned int localPosition=0;
	unsigned int localBufferPosition=Position;
	std::string localInput=Text::currentLine(Input,Position,m_currentLineNumber);
	/*localPosition=0;
	std::cout<<GardenScript::TypeParser::Parser::getInstance()->checkIfType(localInput,localPosition)<<std::endl;
	localPosition=0;
	GardenScript::TypeParser::Parser::getInstance()->parseType(localInput,localPosition);
	localPosition=0;
	GardenScript::Expression::Expression* localExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localInput);
	localPosition=0;*/

	if(localInput.empty())
		return localInput;

	//przekazwac do parsera instruckji cały input zmiast localInput i dawać mu w referencji Position !

	if(GardenScript::Instruction::Parser::getInstance()->checkIfInstruction(localInput,localPosition))
	{
		Position=localBufferPosition;
		return GardenScript::Instruction::Parser::getInstance()->parseInstruction(Input,Position);
	}
	else if(GardenScript::TypeParser::Parser::getInstance()->checkIfType(localInput,localPosition))
		return GardenScript::Declaration::Compiler::getInstance()->compileDeclaration(GardenScript::Declaration::Parser::getInstance()->parseDeclaration(localInput,localPosition));
	else
		return GardenScript::Expression::Compiler::getInstance()->compileExpression(GardenScript::Expression::Parser::getInstance()->parseExpression(localInput));
}

std::string Parser::Parser::parse(std::string Input,unsigned int& Position,unsigned int Offset)
{
	Input.push_back('\0');
	
	std::string localResult;
	m_error=false;

	m_bytecodeSize=Offset;

	while(Position<Input.size()-1)
	{
		try
		{
			m_internalBytecodeSize=0;
			m_beginLineNumber=m_currentLineNumber;

			localResult.append(_parse(Input,Position));

			m_bytecodeSize=localResult.size()+Offset;
			++m_currentLineNumber;
		}
		catch(Exceptions::Exception& e)
		{
			addError(e);
		}
	}

	return localResult;
}

std::string Parser::Parser::internalParse(std::string Input,unsigned int Offset,unsigned int Lines,unsigned int* Position)
{
	static unsigned int staticInstance=0;
	
	unsigned int localInstance=++staticInstance;

	unsigned int localPosition=0;
	Input.push_back('\0');
	
	std::string localResult;
	std::string localResultBuffer;

	m_internalBytecodeSize+=Offset;

	unsigned int localBeginLineNumberBufor=m_beginLineNumber;
	unsigned int localCurrentLineBufor=m_currentLineNumber;

	while(localPosition<Input.size()-1&&Lines--)
	{
		try
		{
			//m_beginLineNumber=m_currentLineNumber;
			++m_beginLineNumber;
			localResultBuffer=_parse(Input,localPosition);

			if(Position)
				*Position=localPosition;

			localResult.append(localResultBuffer.begin(),localResultBuffer.end());

			if(localInstance<staticInstance)
			{
				m_internalBytecodeSize=localResult.size()+Offset;
				--staticInstance;
			}
			else
				m_internalBytecodeSize+=localResultBuffer.size();
			//m_currentLineNumber;
		}
		catch(Exceptions::Exception& e)
		{
			addError(e);
		}
	}

//	m_beginLineNumber=localBeginLineNumberBufor;
	//m_currentLineNumber=localCurrentLineBufor;

	//--staticInstance;

	return localResult;
}

unsigned int Parser::Parser::getCurrentLineNumber()
{
	return m_currentLineNumber;
}

std::string Parser::Parser::getCurrentLineNumberStr(std::string Input,unsigned int Position)
{
	unsigned int localBuffer=0;

	if(Input.empty())
	{
		if(m_currentLineNumber==m_beginLineNumber)
			return Text::stringFormat("%d",m_currentLineNumber);

	//	addWarning("Użyto getCurrentLineNumber bez podania argumentu Input;Position");

		return Text::stringFormat("%d-%d",m_beginLineNumber,m_currentLineNumber);
	}
	else
	{
		for(unsigned int i=0;i<Position;++i)
		{
			if(Input[i]=='\n')
				++localBuffer;
		}

		return Text::stringFormat("%d",localBuffer);
	}
}

bool Parser::Parser::isError()
{
	return m_error;
}

unsigned int Parser::Parser::getBytecodeSize()
{
	return m_bytecodeSize;
}

unsigned int Parser::Parser::getInternalBytecodeSize()
{
	return m_internalBytecodeSize;
}

unsigned int Parser::Parser::getAllBytecodeSize()
{
	return getBytecodeSize()+getInternalBytecodeSize();
}