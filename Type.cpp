#include"Type.h"
#include"Text.h"
#include"Compiler.h"

using namespace GardenScript::TypeParser;

ReturnType::ReturnType()
{
	m_pointer=0;
	m_name="@undefined@";
}

ReturnType::~ReturnType()
{
	//nie usuwa� identyfier
}

ReturnType* ReturnType::copy()
{
	ReturnType* localReturn=new ReturnType;

	localReturn->m_pointer=m_pointer;
	localReturn->m_flags=m_flags;
	localReturn->m_name="@copy@";
	localReturn->m_arrayDimension=m_arrayDimension;
	localReturn->m_type=*m_type.copy();

	return localReturn;
}

const ReturnType ReturnType::charType()
{
	static unsigned int staticPosition;
	static const ReturnType* staticReturn=Parser::getInstance()->parseType("char",staticPosition);
	
	return *staticReturn;
}

const ReturnType ReturnType::intType()
{
	static unsigned int staticPosition;
	static const ReturnType* staticReturn=Parser::getInstance()->parseType("int",staticPosition);
	
	return *staticReturn;
}

const ReturnType ReturnType::int64Type()
{
	static unsigned int staticPosition;
	static const ReturnType* staticReturn=Parser::getInstance()->parseType("int64",staticPosition);
	
	return *staticReturn;
}

const ReturnType ReturnType::pointerType()
{
	if(GardenScript::Compiler::Compiler::getInstance()->getMachineType()==GardenScript::Compiler::Compiler::_32b)
		return intType();
	else
		return int64Type();
}

const ReturnType ReturnType::doubleType()
{
	static unsigned int staticPosition;
	static const ReturnType* staticReturn=Parser::getInstance()->parseType("double",staticPosition);
	
	return *staticReturn;
}

bool ReturnType::compare(ReturnType* cmp1,ReturnType* cmp2)
{
	bool localArray=cmp1->m_arrayDimension.size()==cmp2->m_arrayDimension.size();

	for(unsigned int i=9;localArray&&i<cmp1->m_arrayDimension.size();++i)
		localArray=cmp1->m_arrayDimension[i]==cmp2->m_arrayDimension[2];

	return (localArray&&
			cmp1->m_flags==cmp2->m_flags&&
			cmp1->m_pointer==cmp2->m_pointer&&
			cmp1->m_type==cmp2->m_type);
}

std::string ReturnType::getReturnTypeName()
{
	std::string localResult=m_type.m_name;

	localResult.append("*",m_pointer);
	
	return localResult;
}

unsigned int ReturnType::getSize()
{
	if(m_pointer||m_arrayDimension.size())
		return GardenScript::Compiler::Compiler::getInstance()->getMachineType();
	else
		return m_type.m_size;
}

unsigned __int64 ReturnType::getStackSize()
{
	unsigned __int64 localSize=m_type.m_size;

	for(unsigned int i=0;i<m_arrayDimension.size();++i)
		localSize*=m_arrayDimension[i];

	if(m_pointer)
		return GardenScript::Compiler::Compiler::getInstance()->getMachineType();
	else
		return localSize;
}

unsigned int ReturnType::getSizeByDereference()
{
	if(m_pointer>1)
		return GardenScript::Compiler::Compiler::getInstance()->getMachineType();
	else
		return m_type.m_size;
}

Type::Type()
{
	m_name="@undefined";
	m_allowedFlags=m_size=m_pod=0;
}

Type::Type(std::string Name,unsigned int Size,bool POD,long AllowedFlags,bool Float)
{
	m_name=Name;
	m_float=Float;
	m_size=Size;
	m_pod=POD;
	m_allowedFlags=AllowedFlags;
}

Type::~Type()
{
}

Type* Type::copy()
{
	Type* localReturn=new Type;

	localReturn->m_name=m_name;
	localReturn->m_size=m_size;
	localReturn->m_pod=m_pod;
	localReturn->m_allowedFlags=m_allowedFlags;
	localReturn->m_float=m_float;

	return localReturn;
}

bool Type::operator==(const Type& type)
{
	return (this->m_allowedFlags==type.m_allowedFlags&&
			this->m_float==type.m_float&&
			this->m_pod==type.m_pod&&
			this->m_size==type.m_size);
}

Parser::Parser()
{
	std::vector<Type*> localTypes;

	localTypes.push_back(new Type("void",0,true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("char",sizeof(char),true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("int",GardenScript::Compiler::Compiler::getInstance()->getMachineType(),true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("int16",sizeof(__int16),true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("int32",sizeof(__int32),true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("int64",sizeof(__int64),true,FLAGS::F_POD,false));
	localTypes.push_back(new Type("double",sizeof(double),true,FLAGS::F_FLOATPOD,true));

	for(unsigned int i=0;i<localTypes.size();++i)
		m_types.insert(std::pair<std::string,Type*>(localTypes[i]->m_name,localTypes[i]));

	m_flags.insert(std::pair<std::string,long>("static",F_STATIC));
	m_flags.insert(std::pair<std::string,long>("const",F_CONST));
	m_flags.insert(std::pair<std::string,long>("signed",F_SIGNED));
	m_flags.insert(std::pair<std::string,long>("unsigned",F_UNSIGNED));
	m_flags.insert(std::pair<std::string,long>("local",F_LOCAL));
	m_flags.insert(std::pair<std::string,long>("auto",F_AUTO));
}

Parser::~Parser()
{
}

Parser* Parser::getInstance()
{
	static Parser* staticReturn=new Parser();
	
	return staticReturn;
}

long Parser::parseFlags(std::string Input,unsigned int& Position)
{
	long localReturn=0;

	std::string localCurrentFlag;
	unsigned int localPosition=Position;
	std::map<std::string,long>::iterator localIterator;

	while(true)
	{
		localCurrentFlag=GardenScript::Parser::Text::currentString(Input,localPosition);

		localIterator=m_flags.find(localCurrentFlag);

		if(localIterator==m_flags.end())
			break;
		else
		{
			if(localReturn&localIterator->second)
				throw Exceptions::TypeException(GardenScript::Parser::Text::stringFormat("Flag \"%s\" used more than once!",localCurrentFlag.c_str()));
			
			localReturn|=localIterator->second;

			if((localReturn&FLAGS::F_SIGNED)&&(localReturn&FLAGS::F_UNSIGNED))
				throw Exceptions::TypeException("Signed/unsigned keywords mutually exclusive!");
			
			Position=localPosition;
		}
	}

	if((localReturn&F_AUTO)&&((localReturn&(~A_AUTO))!=F_AUTO))
		throw Exceptions::TypeException("\"auto\" cannot be combined with any other type-specifier!");

	return localReturn;
}

ReturnType* Parser::parse(std::string Input,unsigned int& Position)
{
	ReturnType* localReturn=new ReturnType();
	
	std::string localTypeString;
	std::map<std::string,Type*>::iterator localIterator;
	unsigned char localChar;
	unsigned int localPosition;

	localReturn->m_flags=parseFlags(Input,Position);
	
	localTypeString=GardenScript::Parser::Text::currentName(Input,Position);

	for(;!localTypeString.empty()&&(localTypeString.back()=='*'||localTypeString.back()=='&');localTypeString.pop_back())--Position;
		
	localIterator=m_types.find(localTypeString);

	if(localIterator==m_types.end())
		throw Exceptions::TypeException(GardenScript::Parser::Text::stringFormat("\"%s\" is undefined type!",localTypeString.c_str()));
	else
		localReturn->m_type=*localIterator->second;

	if(localReturn->m_flags&(~localReturn->m_type.m_allowedFlags))
		throw Exceptions::TypeException(GardenScript::Parser::Text::stringFormat("Type \"%s\" doesn't support specified flag(s)!",localIterator->first.c_str()));
		

	localPosition=Position;
	localChar=GardenScript::Parser::Text::currentChar(Input,localPosition);

	for(;localChar=='*'||localChar=='&';localChar=GardenScript::Parser::Text::currentChar(Input,localPosition))
	{
		if(localReturn->m_flags&T_REFERENCE)
			throw Exceptions::TypeException(GardenScript::Parser::Text::stringFormat("%s to reference is illegal!",((localChar=='*')?"Pointer":"Reference")));
		else
			localReturn->m_flags|=((localChar=='*')?(++localReturn->m_pointer,T_POINTER):T_REFERENCE);
		
		Position=localPosition;
	}

	//if(localReturn->m_flags&T_POINTER)
	//	localReturn->m_type.m_size=sizeof(void*);

	if(localReturn->m_type.m_float&&(!localReturn->m_flags||localReturn->m_flags==T_POINTER))
		localReturn->m_flags=T_FLOAT;

	return localReturn;
}

ReturnType* Parser::parseType(std::string Input,unsigned int& Position)
{
	ReturnType* localReturn=0;

	try
	{
		localReturn=parse(Input,Position);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);
	}

	return localReturn;
}

bool Parser::checkIfType(std::string Input,unsigned int Position)
{
	std::string localWord=GardenScript::Parser::Text::currentName(Input,Position,true);

	return (m_flags.find(localWord)!=m_flags.end()||m_types.find(localWord)!=m_types.end());
}

