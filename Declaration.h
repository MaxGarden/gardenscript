#pragma once
#include"Type.h"
#include"Memory.h"
#include"Function.h"

namespace GardenScript
{
	namespace Declaration
	{
		class Parser
		{
		private:
			Parser();
			~Parser();
		private:
			unsigned int parseArguments(std::string Input,Memory::Function* Function);
			Memory::MemoryObject* parseFunction(std::string Input,unsigned int& Position,Memory::MemoryObject* Object,std::string Parameters,Memory::Scope* Scope,bool Argument);
			Memory::MemoryObject* parseVariable(std::string Input,unsigned int& Position,Memory::MemoryObject* Object,Memory::Scope* Scope,bool Argument);
			Memory::MemoryObject* parseArray(std::string Input,unsigned int& Position,Memory::MemoryObject* Object,Memory::Scope* Scope,bool Argument);
			std::vector<GardenScript::Memory::MemoryObject*> parse(std::string Input,unsigned int& Position,Memory::Scope* Scope,bool Argument);
			void assignArraySize(Memory::MemoryObject* Array);
		public:
			static Parser* getInstance();

			std::vector<GardenScript::Memory::MemoryObject*> parseDeclaration(std::string Input,unsigned int& Position,bool Argument=false);
			std::vector<GardenScript::Memory::MemoryObject*> parseDeclaration(std::string Input,unsigned int& Position,Memory::Scope* Scope,bool Argument=false);
		};
	}
}