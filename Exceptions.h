#pragma once
#include<string>

namespace GardenScript
{
	namespace Exceptions
	{
		class Exception
		{
		protected:
			std::string m_message;
			bool m_fatal;
		public:
			Exception(std::string Message,bool Fatal=false);
			virtual ~Exception();

			std::string getMessage();
			bool getFatal();
		};

		class LexicalException:public Exception
		{
		public:
			LexicalException(std::string Message,bool Fatal=false);
			~LexicalException();
		};

		class TextException:public Exception
		{
		public:
			TextException(std::string Message,bool Fatal=false);
			~TextException();
		};

		class ExpressionException:public Exception
		{
		public:
			ExpressionException(std::string Message,bool Fatal=false);
			~ExpressionException();
		};

		class TypeException:public Exception
		{
		public:
			TypeException(std::string Message,bool Fatal=false);
			~TypeException();
		};

		class ParserException:public Exception
		{
		public:
			ParserException(std::string Message,bool Fatal=false);
			~ParserException();
		};

		class MemoryException:public Exception
		{
		public:
			MemoryException(std::string Message,bool Fatal=false);
			~MemoryException();
		};

		class DeclarationException:public Exception
		{
		public:
			DeclarationException(std::string Message,bool Fatal=false);
			~DeclarationException();
		};

		class IdentifierException:public Exception
		{
		public:
			IdentifierException(std::string Message,bool Fatal=false);
			~IdentifierException();
		};

		class OperatorException:public Exception
		{
		public:
			OperatorException(std::string Message,bool Fatal=false);
			~OperatorException();
		};

		class InstructionException:public Exception
		{
		public:
			InstructionException(std::string Message,bool Fatal=false);
			~InstructionException();
		};

		class CompilerException:public Exception
		{
		public:
			CompilerException(std::string Message,bool Fatal=false);
			~CompilerException();
		};

		class ExpressionCompilerException:public Exception
		{
		public:
			ExpressionCompilerException(std::string Message,bool Fatal=false);
			~ExpressionCompilerException();
		};


		class FatalError{};
	}
}