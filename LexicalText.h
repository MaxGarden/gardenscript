#pragma once
#include"Parser.h"

namespace GardenScript
{
	namespace LexicalParser
	{
		namespace Text
		{
			std::string currentToken(std::string Input,unsigned int& Position,bool Update=true);
			std::string currentBody(std::string Input,unsigned int& Position,char Begin,char End,bool Update=true);
		}
	}
}