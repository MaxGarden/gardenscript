#include"Function.h"

using namespace GardenScript;

Memory::Function::Variant::Variant()
{
	m_scope=new Scope();
}

Memory::Function::Variant::~Variant()
{
	for(unsigned int i=0;i<m_arguments.size();++i)
		delete m_arguments[i];

	delete m_scope;
}

Memory::Function::Function()
{
}

Memory::Function::~Function()
{
	for(unsigned int i=0;i<m_variants.size();++i)
		delete m_variants[i];		
}

void Memory::Function::addArgument(unsigned int Variant,TypeParser::ReturnType* Argument)
{
	if(Variant>m_variants.size())
		throw Exceptions::DeclarationException("Variant>m_arguments.size()",true);

	m_variants[Variant]->m_arguments.push_back(Argument);
}

unsigned int Memory::Function::getVariantsSize()
{
	return m_variants.size();
}

void Memory::Function::addVariant()
{
	m_variants.push_back(new Variant());
}

Memory::Scope* Memory::Function::getScope(unsigned int Variant)
{
	return m_variants[Variant]->m_scope;
}

void Memory::Function::setBody(unsigned int Variant,std::string Body)
{
	m_variants[Variant]->m_body=Body;
}

std::string Memory::Function::getBody(unsigned int Variant)
{
	return m_variants[Variant]->m_body;
}

unsigned int Memory::Function::getIndexVariant(std::vector<TypeParser::ReturnType*> Arguments,Scope* scope)
{
	bool localSame=true;
	unsigned int localIndex=0;

	for(unsigned int i=0;i<m_variants.size();++i,localSame=true,localIndex=0)
	{
		if(Arguments.size())
		{
			for(std::vector<TypeParser::ReturnType*>::iterator it=m_variants[i]->m_arguments.begin();it!=m_variants[i]->m_arguments.end()&&localSame;++it,++localIndex)
			{
				localSame=(localIndex<Arguments.size());

				if(localSame=false)
					break;

				//localSame=TypeParser::ReturnType::compare((*it),Arguments[localIndex]); flagi w RT si� nie zgadzaj�
				localSame=((*it)->getReturnTypeName()==Arguments[localIndex]->getReturnTypeName());
			}
		}
		else
			localSame=(Arguments.size()==m_variants[i]->m_arguments.size());

		if(localSame)
			return i;
	}

	return m_variants.size()+1;
}

void Memory::Function::setScope(unsigned int Variant,Scope* scope)
{
	if(m_variants[Variant]->m_scope)
		delete m_variants[Variant]->m_scope;

	m_variants[Variant]->m_scope=scope;
}