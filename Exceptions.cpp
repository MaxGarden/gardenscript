#include"Exceptions.h"
#include"Text.h"
#include"Lexical.h"

using namespace GardenScript;

Exceptions::Exception::Exception(std::string Message,bool Fatal):m_message(Message),m_fatal(Fatal)
{
}

Exceptions::Exception::~Exception()
{
}

std::string Exceptions::Exception::getMessage()
{
	return m_message;
}

bool Exceptions::Exception::getFatal()
{
	return m_fatal;
}

Exceptions::LexicalException::LexicalException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[lexical%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::LexicalException::~LexicalException()
{
}

Exceptions::TextException::TextException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[text%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::TextException::~TextException()
{
}

Exceptions::ExpressionException::ExpressionException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[expression%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::ExpressionException::~ExpressionException()
{
}

Exceptions::TypeException::TypeException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[type%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::TypeException::~TypeException()
{
}

Exceptions::ParserException::ParserException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[parser%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::ParserException::~ParserException()
{
}

Exceptions::MemoryException::MemoryException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[memory%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::MemoryException::~MemoryException()
{
}

Exceptions::DeclarationException::DeclarationException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[declaration%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::DeclarationException::~DeclarationException()
{
}

Exceptions::IdentifierException::IdentifierException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[identifier%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::IdentifierException::~IdentifierException()
{
}

Exceptions::OperatorException::OperatorException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[operator%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::OperatorException::~OperatorException()
{
}

Exceptions::InstructionException::InstructionException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[instruction%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::InstructionException::~InstructionException()
{
}

Exceptions::CompilerException::CompilerException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[compiler%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::CompilerException::~CompilerException()
{
}

Exceptions::ExpressionCompilerException::ExpressionCompilerException(std::string Message,bool Fatal):Exception(Message,Fatal)
{
	m_message=Parser::Text::stringFormat("[expression compiler%s error](%s): %s",((Fatal?" fatal":"")),Parser::Parser::getInstance()->getCurrentLineNumberStr().c_str(),Message.c_str());
}

Exceptions::ExpressionCompilerException::~ExpressionCompilerException()
{
}