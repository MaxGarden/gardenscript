#include"Expression.h"
#include"Text.h"
#include"Constant.h"
#include"Identifier.h"
#include"Memory.h"

using namespace GardenScript;
using namespace Parser;

Expression::Parser::Parser()
{
	m_operators.resize(18);
	m_operatorsAssociativity.resize(18,NONE);

	//0-najni�szy priorytet 17-najwy�szy

#pragma region PRECEDNECE0
	m_operators[0].push_back(",");

	m_operatorsAssociativity[0]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 0

#pragma region PRECEDENCE1
	m_operators[1].push_back("throw"); //osobne

	m_operatorsAssociativity[1]=RIGHT_TO_LEFT|BEGIN;
#pragma endregion 1

#pragma region PRECEDENCE2
	m_operators[2].push_back("=");
	m_operators[2].push_back("+=");
	m_operators[2].push_back("-=");
	m_operators[2].push_back("*=");
	m_operators[2].push_back("/=");
	m_operators[2].push_back("%=");
	m_operators[2].push_back("<<=");
	m_operators[2].push_back(">>=");
	m_operators[2].push_back("&=");
	m_operators[2].push_back("^=");
	m_operators[2].push_back("|=");

	m_operatorsAssociativity[2]=RIGHT_TO_LEFT|MIDDLE;
#pragma endregion 2

#pragma region PRECEDENCE3
	m_operators[3].push_back("?"); //->osobne ----- zrobione

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("?",&ConditionalOperator::operatorFunction));

	m_operatorsAssociativity[3]=RIGHT_TO_LEFT|MIDDLE;
#pragma endregion 3

#pragma region PRECEDENCE4
	m_operators[4].push_back("||");

	m_operatorsAssociativity[4]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 4

#pragma region PRECEDNECE5
	m_operators[5].push_back("&&");

	m_operatorsAssociativity[5]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 5

#pragma region PRECEDENCE6
	m_operators[6].push_back("|");

	m_operatorsAssociativity[6]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 6

#pragma region PRECEDENCE7
	m_operators[7].push_back("^");

	m_operatorsAssociativity[7]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 7

#pragma region PRECEDENCE8
	m_operators[8].push_back("&");

	m_operatorsAssociativity[8]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 8

#pragma region PRECEDENCE9
	m_operators[9].push_back("!=");
	m_operators[9].push_back("==");

	m_operatorsAssociativity[9]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 9

#pragma region PRECEDENCE10
	m_operators[10].push_back(">");
	m_operators[10].push_back(">=");
	m_operators[10].push_back("<");
	m_operators[10].push_back("<=");

	m_operatorsAssociativity[10]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 10

#pragma region PRECEDENCE11
	m_operators[11].push_back(">>");
	m_operators[11].push_back("<<");

	m_operatorsAssociativity[11]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 11

#pragma region PRECEDENCE12
	m_operators[12].push_back("+");
	m_operators[12].push_back("-");

	m_operatorsAssociativity[12]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 12

#pragma region PRECEDENCE13
	m_operators[13].push_back("*");
	m_operators[13].push_back("/");
	m_operators[13].push_back("%");

	m_operatorsAssociativity[13]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 13

#pragma region PRECEDENCE14
	m_operators[14].push_back(".*");
	m_operators[14].push_back("->*");

	m_operatorsAssociativity[14]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 14

#pragma region PRECEDENCE15 
	//right to left
	m_operators[15].push_back("++"); //pre
	m_operators[15].push_back("--"); //pre
	m_operators[15].push_back("+");
	m_operators[15].push_back("-"); //osobno
	m_operators[15].push_back("!");
	m_operators[15].push_back("~");
	m_operators[15].push_back("("); //typecast osobno ----- zrobione
	m_operators[15].push_back("*"); //dereferencja osobno ----- zrobione
	m_operators[15].push_back("&"); //adres osobno ----- zrobione
	m_operators[15].push_back("$"); //osobno ----- zrobione
	m_operators[15].push_back("new"); //osobno
	m_operators[15].push_back("delete"); //osobno
	m_operators[15].push_back("typeid"); //osobno
	//m_operators[15].push_back("const_cast"); //osobno
	m_operators[15].push_back("dynamic_cast"); //osobno
	m_operators[15].push_back("reinterpret_cast"); //osobno
	m_operators[15].push_back("static_cast"); //osobno
	m_operators[15].push_back("{"); //tablica osobno ----- zrobione

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("(",&BinaryOperator::bracketOperatorFunction));

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("*",&DereferenceOperator::operatorFunction));
	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("&",&IdentifierOperator::addressOperatorFunction));

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("$",&ConstantOperator::sizeofOperatorFunction));
	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("-",&ConstantOperator::unaryMinusOperatorFunction));

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("{",&StaticDataOperator::operatorFunction));

	m_operatorsAssociativity[15]=RIGHT_TO_LEFT|BEGIN;
#pragma endregion 15

#pragma region PRECEDENCE16
	m_operators[16].push_back("++"); //post
	m_operators[16].push_back("--"); //post
	m_operators[16].push_back("("); //call osobno
	m_operators[16].push_back("["); //array osobno ----- zrobione
	m_operators[16].push_back("."); 
	m_operators[16].push_back("->"); 

	m_operatorsFunctions.insert(std::pair<std::string,OperatorFunction>("[",&BinaryOperator::arrayOperatorFunction));

	m_operatorsAssociativity[16]=LEFT_TO_RIGHT|MIDDLE;
#pragma endregion 16

#pragma region PRECEDENCE17
	m_operators[17].push_back("::"); //scope osobno

	m_operatorsAssociativity[17]=NONE;
#pragma endregion 17



}

Expression::Parser::~Parser()
{
}

Expression::Parser* Expression::Parser::getInstance()
{
	static Parser* staticReturn=new Parser();

	return staticReturn;
}

bool Expression::Parser::isFloat(TypeParser::ReturnType type)
{
	return !type.m_pointer&&((type.m_flags&TypeParser::T_FLOAT)!=0);
}

bool Expression::Parser::isFloat(Expression* expression)
{
	std::vector<GardenScript::TypeParser::ReturnType> localTypes;

	if(!expression)
		return false;

	if(expression->m_type==Expression::CONSTANT)
		return ((ConstantOperator*)expression)->getIsFloat();
	else if(expression->m_type==Expression::BINARY)
		return isFloat(((BinaryOperator*)expression)->m_left)||isFloat(((BinaryOperator*)expression)->m_right);

	localTypes=GardenScript::Expression::Parser::getInstance()->getTypesOfExpression(expression);

	return GardenScript::Expression::Parser::getInstance()->isFlagType(localTypes,GardenScript::TypeParser::T_FLOAT);
}


char Expression::Parser::getOperatorID(std::string Operator,long Associativity)
{
	char localReturn=0;
	char localID=0;

	for(unsigned int i=0;i<m_operators.size();++i)
	{
		for(unsigned int x=0;x<m_operators[i].size();++x)
		{
			++localID;
			if(m_operators[i][x]==Operator&&(!Associativity||getOperatorAssociativity(localID)&Associativity))
				return localReturn;
			++localReturn;
		}
	}

	return -1;
}

char Expression::Parser::getOperatorID(unsigned char Precedence,unsigned char Index)
{
	char localReturn=0;

	for(unsigned int i=0;i<Precedence&&i<m_operators.size();++i)
		localReturn+=(char)m_operators[i].size();

	return localReturn+Index;
}

long Expression::Parser::getOperatorAssociativity(unsigned char Operator)
{
	unsigned char localBuffer=0;

	for(unsigned int i=0;i<m_operators.size();++i)
	{
		if(localBuffer>=Operator)
			return m_operatorsAssociativity[i];

		localBuffer+=(unsigned char)m_operators[i+1].size();
	}

	throw Exceptions::ExpressionException("Operator doesn't exsist!");
}

bool Expression::Parser::isModifyingOperator(unsigned char Operator)
{
	std::string localOperatorString=getOperatorString(Operator);

	if(localOperatorString.size()==1&&localOperatorString!="=")
		return false;

	if(getOperatorAssociativity(Operator)&RIGHT_TO_LEFT)
		return true;
	
	return (getOperatorString(Operator)=="++"||getOperatorString(Operator)=="--");
}

std::vector<GardenScript::TypeParser::ReturnType> Expression::Parser::getTypesOfExpression(Expression* expression)
{
	std::vector<GardenScript::TypeParser::ReturnType> localReturn;
	std::vector<GardenScript::TypeParser::ReturnType> localBuffer;

	if(!expression)
		return localReturn;

	if(expression->m_type==Expression::BINARY)
	{
		localBuffer=getTypesOfExpression(((BinaryOperator*)expression)->m_left);
		localReturn.insert(localReturn.begin(),localBuffer.begin(),localBuffer.end());
		localBuffer=getTypesOfExpression(((BinaryOperator*)expression)->m_right);
		localReturn.insert(localReturn.begin(),localBuffer.begin(),localBuffer.end());
	}
	else if(expression->m_type==Expression::IDENTIFIER)
		localReturn.push_back(((IdentifierOperator*)expression)->getObject()->m_type);
	else if(expression->m_type==Expression::DEREFERENCE)
		localReturn.push_back(((DereferenceOperator*)expression)->getObject()->m_type);

	return localReturn;
}

bool Expression::Parser::isFlagType(std::vector<GardenScript::TypeParser::ReturnType> Types,long Flag)
{
	for(unsigned int i=0;i<Types.size();++i)
		if(Types[i].m_flags&Flag)
			return true;
	
	return false;
}

Expression::Expression* Expression::Parser::parse(std::string Input,unsigned int& Position,unsigned int Precence)
{
	Expression* localExpression=0;

	std::string localOperator;
	unsigned int localPosition=0;
	unsigned int localBufferPosition;
	char localOperatorID;

	if(Precence<m_operators.size()-1)
	{
		localExpression=parse(Input,Position,Precence+1);

		if(m_operatorsAssociativity[Precence]&BEGIN)
			return localExpression;

		localBufferPosition=Position;
		localOperator=GardenScript::Parser::Text::currentOperator(Input,localBufferPosition,MIDDLE,&localOperatorID);

		while(localOperator.size()&&localOperator.back()=='\0')
			localOperator.pop_back();

		for(unsigned int i=0;i<m_operators[Precence].size();++i)
		{
			while(localOperator==m_operators[Precence][i])
			{
				localPosition=0;
				Position=localBufferPosition;

				if((m_operators[Precence][i].size()>1||!GardenScript::Parser::Text::checkChar(m_operators[Precence][i][0],"-+*"))&&m_operatorsFunctions.find(m_operators[Precence][i])!=m_operatorsFunctions.end())
					localExpression=m_operatorsFunctions.find(m_operators[Precence][i])->second(localExpression,Input,Position);
				else
					localExpression=new BinaryOperator(localOperatorID,localExpression,parse(Input,Position,Precence+1));

				localBufferPosition=Position;

				localOperator=GardenScript::Parser::Text::currentOperator(Input,localBufferPosition,MIDDLE,&localOperatorID);

				i=0;
			}
		}
	

		if(Precence==0&&Text::currentChar(Input,Position,false)!='\0')
			throw Exceptions::ExpressionException(Text::stringFormat("Syntax error \"%s\"",Text::cutString(Input,Position).c_str()));

	}
	else
		localExpression=identifier(Input,Position);

	

	return localExpression;
}

Expression::Expression* Expression::Parser::identifier(std::string Input,unsigned int& Position)
{
	std::string localBracets;
	char localOperatorID;
	GardenScript::Parser::Text::adjust(Input,Position);
	std::string localOperator=GardenScript::Parser::Text::currentOperator(Input,Position,BEGIN,&localOperatorID);
	unsigned int localPosition=0;

	for(unsigned int i=0;i<m_operatorsAssociativity.size();++i)
	{
		if(!(m_operatorsAssociativity[i]&BEGIN))
			continue;

		for(unsigned int x=0;x<m_operators[i].size();++x)
		{
			if(localOperator==m_operators[i][x])
			{
				if(m_operatorsFunctions.find(m_operators[i][x])!=m_operatorsFunctions.end())
					return m_operatorsFunctions.find(m_operators[i][x])->second(0,Input,Position);
				else
					return new BinaryOperator(localOperatorID,identifier(Input,Position),0);
			}
		}
	}

	if(isdigit(Input[Position]))
		return ConstantParser::getInstance()->parseNumbers(Input,Position);
	else if(Input[Position]=='\''||Input[Position]=='\"')
		return ConstantParser::getInstance()->parseChars(Input,Position);
	else if(GardenScript::Parser::Text::isName(std::string(1,Input[Position])))
		return IdentifierParser::getInstance()->parse(Input,Position);
	else if(Position<Input.size()&&Input[Position]=='\0')
		return 0;
	else
		throw Exceptions::ExpressionException(Text::stringFormat("Unidentified something \"%s\"!",Text::cutString(Input,Position).c_str()));
}

Expression::Expression* Expression::Parser::parseExpression(std::string Input)
{
	unsigned int localPosition=0;
	Expression* localReturn=0;

	try
	{
		Input+='\0';
		localReturn=parse(Input,localPosition);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);
	}

	return localReturn;
}

#ifdef __GS_DEBUG_
std::string Expression::Parser::getOperatorStr(unsigned char ID)
{
	for(unsigned int i=0;i<=17;++i)
		for(unsigned int x=0;x<m_operators[i].size();++x,--ID)
			if(ID==0)
				return m_operators[i][x];
	
	return "";
}
#endif

std::string Expression::Parser::getOperatorString(unsigned char ID)
{
	for(unsigned int i=0;i<=17;++i)
		for(unsigned int x=0;x<m_operators[i].size();++x,--ID)
			if(ID==0)
				return m_operators[i][x];
	
	return "";
}

__int64 Expression::Parser::calculateConstantExpression(Expression* expression,unsigned int* Size)
{
	__int64 localResult=0;

	if(expression->m_type!=Expression::CONSTANT)
		throw Exceptions::ExpressionException("expression->m_type!=Expression::CONSTANT <- nie sko�czone");

	memcpy(&localResult,((ConstantOperator*)expression)->m_value,((ConstantOperator*)expression)->m_size);

	if(Size)
		*Size=((ConstantOperator*)expression)->m_size;

	return localResult;
}

Expression::BinaryOperator::BinaryOperator(char Operator,Expression* Left,Expression* Right)
{
#ifdef __GS_DEBUG_
	m_debugOperator=GardenScript::Expression::Parser::getInstance()-> getOperatorStr(Operator);
#endif

	m_operator=Operator;
	m_left=Left;
	m_right=Right;

	m_type=BINARY;
}

Expression::BinaryOperator::~BinaryOperator()
{

}

char Expression::BinaryOperator::getOperator()
{
	return m_operator;
}


GardenScript::TypeParser::ReturnType Expression::BinaryOperator::calculateReturnType()
{
	TypeParser::ReturnType localResult;
	std::string localOperatorString=Parser::getInstance()->getOperatorString(getOperator());

	if(localOperatorString=="&")
		return TypeParser::ReturnType::pointerType();
	if((localOperatorString.size()==1&&Text::checkChar(localOperatorString[0],"<>"))||(localOperatorString.size()>1&&localOperatorString[1]=='='))
		return TypeParser::ReturnType::charType();

	return m_left->calculateReturnType();
}

Expression::ConstantOperator::ConstantOperator(void* Value,unsigned int Size,TypeParser::ReturnType Type)
{
	m_size=Size;
	m_returnType=Type;

	m_value=new char[m_size];
	memcpy(m_value,Value,m_size);

	m_type=CONSTANT;
}

Expression::ConstantOperator::~ConstantOperator()
{
	delete m_value;
}

bool Expression::ConstantOperator::getIsFloat()
{
	return (m_returnType.m_flags&TypeParser::T_FLOAT)!=0;
}

GardenScript::TypeParser::ReturnType Expression::ConstantOperator::calculateReturnType()
{
	return m_returnType;
}
