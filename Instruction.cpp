#include"Instruction.h"
#include"Exceptions.h"
#include"Expression.h"
#include"Compiler.h"
#include"DeclarationCompiler.h"
#include"InstructionCompiler.h"

using namespace GardenScript::Instruction;

Parser::Parser()
{
	m_keywords.insert(std::pair<std::string,InstructionFunction>("if",parseIf));
	m_keywords.insert(std::pair<std::string,InstructionFunction>("while",parseWhile));
	m_keywords.insert(std::pair<std::string,InstructionFunction>("do",parseDoWhile));
	m_keywords.insert(std::pair<std::string,InstructionFunction>("for",parseFor));

	m_keywords.insert(std::pair<std::string,InstructionFunction>("break",parseBreak));
	m_keywords.insert(std::pair<std::string,InstructionFunction>("continue",parseContinue));

	m_keywords.insert(std::pair<std::string,InstructionFunction>("return",parseReturn));

	m_keywords.insert(std::pair<std::string,InstructionFunction>("print",parsePrint));
}

Parser::~Parser()
{
}

Parser* Parser::getInstance()
{
	static Parser* staticReturn=new Parser();
	
	return staticReturn;
}

std::string Parser::parse(std::string Input,unsigned int& Position)
{
	std::string localInstruction=GardenScript::Parser::Text::currentName(Input,Position);
	std::map<std::string,InstructionFunction>::iterator localIterator=m_keywords.find(localInstruction);

	if(localIterator==m_keywords.end())
		throw Exceptions::InstructionException(GardenScript::Parser::Text::stringFormat("\"%s\" is undefined instruction!",localInstruction.c_str()));
	else
	{
		GardenScript::Parser::Text::adjust(Input,Position);
		return localIterator->second(Input,Position);
	}
}

std::string Parser::parseInstruction(std::string Input,unsigned int& Position)
{
	std::string localResult;

	try
	{
		localResult=parse(Input+';',Position);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);
	}

	return localResult;
}

bool Parser::checkIfInstruction(std::string Input,unsigned int Position)
{
	std::string localInstruction=GardenScript::Parser::Text::currentName(Input,Position);
	std::map<std::string,InstructionFunction>::iterator localIterator=m_keywords.find(localInstruction);

	return localIterator!=m_keywords.end();
}

std::string Parser::parseElse(std::string Input,unsigned int& Position,bool Self)
{
	std::string localResult;

	if(GardenScript::Parser::Text::currentName(Input,Position,false)!="else")
		return localResult;

	if(Self)
		localResult="else ";

	GardenScript::Parser::Text::currentName(Input,Position,true);

	if(GardenScript::Parser::Text::currentName(Input,Position,false)=="if")
	{
		localResult+=GardenScript::Parser::Text::currentName(Input,Position,true);
		
		if(GardenScript::Parser::Text::currentChar(Input,Position,false)!='(')
			throw Exceptions::InstructionException("Expected \"(\"!");

		GardenScript::Parser::Text::adjust(Input,Position);
		localResult+=GardenScript::Parser::Text::stringFormat("(%s)",GardenScript::Parser::Text::currentBody(Input,++Position,'(',')',1).c_str());

		localResult+=GardenScript::Parser::Text::currentLine(Input,Position,true,true);

		return localResult+parseElse(Input,Position,true);
		
	}

	return GardenScript::Parser::Text::currentLine(Input,Position);
}

std::string Parser::parseIf(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!='(')
		throw Exceptions::InstructionException("Expected \"(\"!");

	std::string localIfCondition=GardenScript::Parser::Text::currentBody(Input,++Position,'(',')',1);
	std::string localIfBody;

	GardenScript::Expression::Expression* localIfExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localIfCondition);

	if(GardenScript::Parser::Text::currentChar(Input,Position,false)=='{')
	{
		GardenScript::Parser::Text::currentChar(Input,Position);
		localIfBody=GardenScript::Parser::Text::currentBody(Input,Position,'{','}',1);
	}
	
	return Instruction::Compiler::getInstance()->compileIfInstruction(localIfExpression,GardenScript::Parser::Text::cutString(Input,Position),Position,localIfBody);
}

std::string Parser::parseWhile(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!='(')
		throw Exceptions::InstructionException("Expected \"(\"!");

	std::string localWhileCondition=GardenScript::Parser::Text::currentBody(Input,++Position,'(',')',1);

	GardenScript::Expression::Expression* localWhileExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localWhileCondition);

	return Instruction::Compiler::getInstance()->compileWhileInstruction(localWhileExpression,GardenScript::Parser::Text::cutString(Input,Position),Position);
}

std::string Parser::parseDoWhile(std::string Input,unsigned int& Position)
{
	std::string localWhileCondition;
	std::string localBody;
	GardenScript::Expression::Expression* localWhileExpression;

	if(GardenScript::Parser::Text::currentChar(Input,Position,false)=='{')
	{
		GardenScript::Parser::Text::currentChar(Input,Position);
		localBody=GardenScript::Parser::Text::currentBody(Input,Position,'{','}',1);
	}
	else
		throw Exceptions::InstructionException("Expected \"{\"!");

	if(GardenScript::Parser::Text::currentName(Input,Position,false)=="while")
	{
		GardenScript::Parser::Text::currentName(Input,Position);

		if(GardenScript::Parser::Text::currentChar(Input,Position,false)!='(')
			throw Exceptions::InstructionException("Expected \"(\"!");

		GardenScript::Parser::Text::currentChar(Input,Position);
		localWhileCondition=GardenScript::Parser::Text::currentBody(Input,Position,'(',')',1);

		localWhileExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localWhileCondition);
	}
	else
		throw Exceptions::InstructionException("Expected \"while\"!");

	return Instruction::Compiler::getInstance()->compileDoWhileInstruction(localWhileExpression,localBody);
}

std::string Parser::parseFor(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!='(')
		throw Exceptions::InstructionException("Expected \"(\"!");

	GardenScript::Parser::Text::currentChar(Input,Position,true);

	std::string localForDefinition=GardenScript::Parser::Text::currentBody(Input,Position,'(',')',1);
	unsigned int localPosition=0;

	std::string localForDeclaration=GardenScript::Parser::Text::currentLine(localForDefinition,localPosition);
	std::string localForCondition=GardenScript::Parser::Text::currentLine(localForDefinition,localPosition);
	std::string localForCycle=GardenScript::Parser::Text::cutString(localForDefinition,localPosition);

	return Instruction::Compiler::getInstance()->compileForInstruction(localForDeclaration,localForCondition,localForCycle,GardenScript::Parser::Text::cutString(Input,Position),Position);
}

std::string Parser::parseBreak(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!=';')
		throw Exceptions::InstructionException("Expected \";\"!");

	GardenScript::Parser::Text::currentChar(Input,Position);

	return Instruction::Compiler::getInstance()->compileBreakInstruction();
}

std::string Parser::parseContinue(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!=';')
		throw Exceptions::InstructionException("Expected \";\"!");

	GardenScript::Parser::Text::currentChar(Input,Position);

	return Instruction::Compiler::getInstance()->compileContinueInstruction();
}

std::string Parser::parsePrint(std::string Input,unsigned int& Position)
{
	if(GardenScript::Parser::Text::currentChar(Input,Position,false)==';')
		throw Exceptions::InstructionException("Expected expression!");
	
	std::string localExpressionStr=GardenScript::Parser::Text::currentExpression(Input,Position,';');

	GardenScript::Expression::Expression* localPrintExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localExpressionStr);

	return Instruction::Compiler::getInstance()->compilePrintInstruction(localPrintExpression);
}

std::string Parser::parseReturn(std::string Input,unsigned int& Position)
{
	std::string localExpressionStr;
	GardenScript::Expression::Expression* localReturnExpression=0;

	if(GardenScript::Parser::Text::currentChar(Input,Position,false)!=';')
	{
		localExpressionStr=GardenScript::Parser::Text::currentExpression(Input,Position,';');

		localReturnExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localExpressionStr);
	}

	return Instruction::Compiler::getInstance()->compileReturnInstruction(localReturnExpression);

}