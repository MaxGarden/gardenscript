#include"LexicalText.h"
#include"Text.h"
#include"Exceptions.h"

using namespace GardenScript;
using namespace GardenScript::LexicalParser;
using namespace GardenScript::Parser::Text;

std::string Text::currentToken(std::string Input,unsigned int& Position,bool Update)
{
	char localChar;
	std::string localReturn="";
	unsigned int localPosition=Position;

	if(localPosition>=Input.size())
		return ";";

	localChar=currentChar(Input,localPosition,false);

	std::string localStringChar;
	localStringChar=localChar;

	if(localChar==';')
		localReturn=';';
	else if(localChar=='\'')
	{
		localReturn='\'';
		localReturn+=currentBody(Input,localPosition,'\'','\'');
		localReturn+='\'';

		if(Update)
			Position=localPosition;

		if(localReturn.size()>3)
			throw Exceptions::LexicalException("Too many characters in constant");
		else if(localReturn.size()==2)	
			throw Exceptions::LexicalException("Empty character constant");
		
	}
	else if(localChar=='\"')
	{
		localReturn='\"';
		localReturn+=currentBody(Input,localPosition,'\"','\"');
		localReturn+='\"';
	}
	else if(localChar=='(')
	{
		localReturn='(';
		localReturn+=currentBody(Input,localPosition,'(',')');
		localReturn+=')';
	}
	else if(localChar=='[')
	{
		localReturn='[';
		localReturn+=currentBody(Input,localPosition,'[',']');
		localReturn+=']';
	}
	else
	{
		localReturn=currentChar(Input,localPosition);
		localReturn+=currentChar(Input,localPosition,false);

		if(checkString(localReturn,Parser::Parser::getInstance()->m_stringOperators))
		{
			localChar='\0';
			currentChar(Input,localPosition,true);
		}
		else
		{
			localReturn.pop_back();

			if(checkString(localReturn,Parser::Parser::getInstance()->m_stringOperators))
				localChar='\0';
			else
				localReturn.pop_back();
		}

	}
	
	if(localReturn=="")
	{
		if(isdigit(localChar))
		{
			while(isdigit(localChar))
			{
				localReturn+=localChar;
				localChar=currentChar(Input,localPosition);
			}

			--localPosition;

			if(!checkChar(localChar,constDoubleArgumentsOperators)&&localChar!=constEndExpressionChar)
			{
				if(Update)
					Position=++localPosition;
				
				throw Exceptions::LexicalException(stringFormat("Unrecognized token \"%s%c\"",localReturn.c_str(),localChar));
			}
		}
		else
		{
			for(;!isspace(localChar)&&(isalpha(localChar)||isdigit(localChar))||checkChar(localChar,constAllowedNameChars);localChar=currentChar(Input,localPosition))
				localReturn+=localChar;

			--localPosition;
		}
	}

	if(localReturn=="")
	{
		if(Update)
			Position=++localPosition;

		throw Exceptions::LexicalException(stringFormat("Unrecognized token \"%c\"",localChar));
	}

	if(Update)
		Position=localPosition;

	return localReturn;
}


std::string Text::currentBody(std::string Input,unsigned int& Position,char Begin,char End,bool Update)
{
	int localTerms=0;

	std::string localBody="";
	char localChar;

	unsigned int localPosition=Position;

	while(localPosition<Input.size())
	{
		localChar=Input[localPosition];

		if(Begin!=End)
		{
			if(localChar==Begin)
			{
				++localTerms;

				if(localTerms==1)
				{
					++localPosition;
					continue;
				}
			}
			else if(localChar==End)
				--localTerms;
		}
		else
		{
			if(localChar==Begin)
			{
				if(localTerms==0)
				{
					++localTerms;
					++localPosition;
					continue;
				}
				else
				{
					--localTerms;
					break;
				}
			}
		}

		if(localTerms<=0)
			break;

		localBody+=localChar;

		++localPosition;
	}
	++localPosition;

	if(Update)
		Position=localPosition;

	if(localTerms)
		throw Exceptions::LexicalException(Parser::Text::stringFormat("Expected a \'%c\'",End));

	return localBody;
}