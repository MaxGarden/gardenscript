#pragma once
#include"Text.h"
#include"Parser.h"
#include"Bytecode.h"
#include"Type.h"
#include<map>

namespace GardenScript
{
	namespace Memory
	{
		struct MemoryObject;
		class Function;
	}

	namespace Declaration
	{
		class Compiler;
	}

	namespace Expression
	{
		class Parser;
		class Compiler;

		class Expression 
		{
		public:
			enum TYPE
			{
				CONSTANT,
				BINARY,
				IDENTIFIER,
				DEREFERENCE,
				STATIC_DATA,
				MEMBEROP,
				NEW,
				ARRAY,
				CALLOP,
				STATIC
			};

			TYPE m_type;
			TypeParser::ReturnType m_returnType;
		public:
			Expression():m_const(false){};
			virtual ~Expression(){};

			bool m_const;

			virtual GardenScript::TypeParser::ReturnType calculateReturnType()=0;
			//virtual ReturnType check()=0;
		};

		class ConstantOperator:public Expression
		{
		friend Parser;
		friend Compiler;
		friend Declaration::Compiler;
		protected:
			void* m_value;
			unsigned int m_size;
		public:
			ConstantOperator(void* Value,unsigned int Size,TypeParser::ReturnType Type);
			virtual ~ConstantOperator();

			bool getIsFloat();
			//virtual ReturnType check();

			static Expression* sizeofOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
			static Expression* unaryMinusOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
		
			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class BinaryOperator:public Expression
		{
		public:
			static const unsigned char s_multiplicationAssignOperator=5;
			static const unsigned char s_additionOperator=27;
			static const unsigned char s_subtractionOperator=28;
			static const unsigned char s_multiplicationOperator=29;
			static const unsigned char s_unaryMinusOperator=37;
			static const unsigned char s_addressOperator=42;
		protected:
			unsigned char m_operator;
			std::string m_parameters;
		public:
			Expression* m_left;
			Expression* m_right;

#ifdef __GS_DEBUG_
			std::string m_debugOperator;
#endif

			BinaryOperator(char Operator,Expression* Left,Expression* Right);
			virtual ~BinaryOperator();

		//	virtual ReturnType check();

			char getOperator();

			static Expression* arrayOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
			static Expression* bracketOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);

			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class IdentifierOperator:public Expression
		{
		protected:
			Expression* m_expression;
			Memory::MemoryObject* m_object; 
		public:
			IdentifierOperator(Memory::MemoryObject* Object,Expression* expression=0);
			virtual ~IdentifierOperator();

			Memory::MemoryObject* getObject();
			Expression* getExpression();
		//	virtual ReturnType check();

			static Expression* addressOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
		
			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};



		class ConditionalOperator:public Expression
		{
		protected:
			Expression* m_condition;
			Expression* m_firstExpression;
			Expression* m_secondExpression;
		public:
			ConditionalOperator(Expression* Condition,Expression* FirstExpression,Expression* SecondExpression);
			virtual ~ConditionalOperator();

			static Expression* operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
		
			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class TypecastOperator:public Expression
		{
		protected:
			TypeParser::ReturnType* m_type;
			Expression* m_expression;
		private:
			TypecastOperator(TypeParser::ReturnType* Type);
			virtual ~TypecastOperator();
		public:
			static Expression* operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
		
			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class DereferenceOperator:public IdentifierOperator
		{
		protected:
			Expression* m_dereferenceExpression;
			unsigned char m_operator;
		private:
			DereferenceOperator(Expression* Expression,IdentifierOperator* Pointer,unsigned char Operator);
			virtual ~DereferenceOperator();

			static IdentifierOperator* findPointerOrArray(BinaryOperator* Binary,unsigned char& Operator,Expression** Parent=0);
		public:
			static Expression* operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
		
			Expression* getDereferenceExpression();
			unsigned char getOperator();

			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class StaticDataOperator:public Expression
		{
		protected:
			void* m_data;
			unsigned int m_size;
			std::vector<unsigned int> m_elementsSize;
		public:
			StaticDataOperator(void* PointerToData,unsigned int Size,std::vector<unsigned int> ElementsSize);
			virtual ~StaticDataOperator();

			void* createAdjutedData(unsigned int ElementSize,unsigned int& DataSize);
			unsigned int getSize(unsigned int ElementSize=0);

			static Expression* operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);
			
			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		class CallOperator:public Expression
		{
		protected:
			unsigned int m_variant;
			Memory::Function* m_function;
			Expression* m_functionExpression;
			std::vector<Expression*> m_arguments;
		private:
			static std::string argumentsReturnTypeNamesToString(std::vector<TypeParser::ReturnType*> Arguemnts);
			static std::vector<Expression*> parseArguments(std::string Input);
		public:
			CallOperator();
			virtual ~CallOperator();

			static Expression* operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position);

			virtual GardenScript::TypeParser::ReturnType calculateReturnType();
		};

		/*class MemberOperator:public Expression
		{
		protected:
			int m_variant;
			unsigned int m_iD;
			std::vector<Expression*> m_parameters;
			Object* m_union;
			Object* m_object;
		public:
			MemberOperator(Object* _Object,Object* _Union);
			MemberOperator(unsigned int ID,int Variant,std::vector<Expression*> Parameters,Object* _Union);
			virtual ~MemberOperator();

		//	virtual ReturnType check();
		};

		class NewOperator:public Expression
		{
		protected:
			Object* m_object;
		};

		

		class CallOperator:public Expression
		{
		protected:
			Expression* m_expression;
			std::vector<Expression*> m_parameters;
			Memory* m_memory;
			unsigned int m_variant;
			bool m_function;
		public:
			CallOperator(Expression* _Expression,std::vector<Expression*> Parameters);
			virtual ~CallOperator();

		//	virtual ReturnType check();
		};

		class StaticMemberOperator:public Expression
		{
		protected:
			Expression* m_base;
			std::vector<std::string> m_objects;
			Object* m_object;
		public:
			StaticMemberOperator(Expression* Base,std::vector<std::string> Objects);
			virtual ~StaticMemberOperator();

		//	virtual ReturnType check();
		};*/


		class Parser
		{
		friend Compiler;
		friend IdentifierOperator;
		friend DereferenceOperator;
		friend ConstantOperator;
		friend TypecastOperator;
		private:
			Parser();
			~Parser();
		protected:
			enum ASSOCIATIVITY
			{
				NONE,
				LEFT_TO_RIGHT=0xf000,
				RIGHT_TO_LEFT=0x0f00,
				BEGIN=0x00f0,
				MIDDLE=0x000f
			};
		protected:
			std::vector<std::vector<std::string> > m_operators;
			std::vector<long> m_operatorsAssociativity;
			typedef Expression*(*OperatorFunction)(Expression* Identifier,std::string Input,unsigned int& Position);

			std::map<std::string,OperatorFunction> m_operatorsFunctions;
		public:
			bool isFloat(Expression* expression);
			bool isFloat(TypeParser::ReturnType type);
		private:
			Expression* parse(std::string Input,unsigned int& Position,unsigned int Precence=0);
			Expression* identifier(std::string Input,unsigned int& Position);
		public:
			char getOperatorID(std::string Operator,long Associativity);
			char getOperatorID(unsigned char Precedence,unsigned char Index);

			long getOperatorAssociativity(unsigned char Operator);
			bool isModifyingOperator(unsigned char Operator);
			std::string getOperatorString(unsigned char Operator);
			__int64 calculateConstantExpression(Expression* expression,unsigned int* Size=0);
			std::vector<GardenScript::TypeParser::ReturnType> getTypesOfExpression(Expression* expression);
			bool isFlagType(std::vector<GardenScript::TypeParser::ReturnType> Types,long Flag);
		public:

#ifdef __GS_DEBUG_
			std::string getOperatorStr(unsigned char ID);
#endif

			static Parser* getInstance();

			Expression* parseExpression(std::string Input);
		};
	}
}
	
