﻿#include"InstructionCompiler.h"
#include"ExpressionCompiler.h"
#include"DeclarationCompiler.h"
#include"Type.h"
#include<Assembler.h>

using namespace GardenScript;


Instruction::Compiler::Compiler()
{
}

Instruction::Compiler::~Compiler()
{
}

Instruction::Compiler* Instruction::Compiler::getInstance()
{
	static Compiler* staticReturn=new Compiler();

	return staticReturn;
}

Instruction::bytecode Instruction::Compiler::compileIfInstruction(Expression::Expression* Condition,std::string Input,unsigned int& Position,std::string IfBody)
{
	bytecode localResult;
	bytecode localBody;
	unsigned int localIfJumpSize=0;
	unsigned int localInternalResultSize=0;
	unsigned int localPosition=0;
	unsigned int localAbsolutePosition=0;
	unsigned int localBodyOffset;
	const unsigned int localConstantSize=GardenScript::Compiler::Compiler::getInstance()->adjustConstant(0).size();

	localResult+=GardenScript::Expression::Compiler::getInstance()->compileExpression(Condition);
	
	localResult.push_back(Assembler::IFA);
 
	GardenScript::Declaration::Compiler::getInstance()->pushScope(localBody);
	
	localInternalResultSize=localResult.size()+localConstantSize+localBody.size();

	localBodyOffset=localBody.size();

	if(IfBody.size())
		localBody+=GardenScript::Parser::Parser::getInstance()->internalParse(IfBody,localInternalResultSize);
	else
		localBody+=GardenScript::Parser::Parser::getInstance()->internalParse(Input,localInternalResultSize,1,&localPosition);

	localBodyOffset=localBody.size()-localBodyOffset;

	localAbsolutePosition=localPosition;

	GardenScript::Declaration::Compiler::getInstance()->popScope(localBody);

	localIfJumpSize=localConstantSize+1;

	if(GardenScript::Parser::Text::currentName(Input,localAbsolutePosition,false)=="else")
		localIfJumpSize+=localConstantSize+1;
		
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(localBody.size()+localIfJumpSize));

	localResult+=localBody;

	if(localIfJumpSize>localConstantSize+1)
	{
		GardenScript::Parser::Text::currentName(Input,localAbsolutePosition);

		localResult.push_back(Assembler::JPD);
		localBody="";
		localPosition=0;

		GardenScript::Declaration::Compiler::getInstance()->pushScope(localBody);
		
		localInternalResultSize=localResult.size()-localInternalResultSize+localBody.size()+localConstantSize-localBodyOffset;

		if(GardenScript::Parser::Text::currentChar(Input,localAbsolutePosition,false)=='{')
		{
			GardenScript::Parser::Text::currentChar(Input,localAbsolutePosition);
			localBody+=GardenScript::Parser::Parser::getInstance()->internalParse(GardenScript::Parser::Text::currentBody(Input,localAbsolutePosition,'{','}',1),localInternalResultSize);
		}
		else
			localBody+=GardenScript::Parser::Parser::getInstance()->internalParse(GardenScript::Parser::Text::cutString(Input,localAbsolutePosition),localInternalResultSize,1,&localPosition);

		localAbsolutePosition+=localPosition;

		GardenScript::Declaration::Compiler::getInstance()->popScope(localBody);

		localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localBody.size()));

		localResult+=localBody;
	}

	Position+=localAbsolutePosition;

	return localResult;
}

void Instruction::Compiler::initLoop(bytecode& Loop)
{
	m_breaksOffsets.push_back(std::vector<unsigned int>());
	m_breaksOffsets.back().push_back(GardenScript::Parser::Parser::getInstance()->getAllBytecodeSize());

	GardenScript::Declaration::Compiler::getInstance()->pushScope(Loop);
}

void Instruction::Compiler::beginLoop(bytecode& Loop,unsigned int Offset)
{
	m_loopBeginOffsets.push_back(GardenScript::Parser::Parser::getInstance()->getAllBytecodeSize()+Loop.size()+Offset);
}

Instruction::bytecode Instruction::Compiler::compileBodyLoop(std::string Input,unsigned int& Position,bytecode Begin,bool Condition)
{
	bytecode localResult;
	unsigned int localInternalResultSize=0;
	unsigned int localPosition=0;
	const unsigned int localConstantSize=GardenScript::Compiler::Compiler::getInstance()->adjustConstant(0).size();

	localInternalResultSize=Begin.size()+((Condition)?localConstantSize:0);

	if(GardenScript::Parser::Text::currentChar(Input,localPosition,false)=='{')
	{
		GardenScript::Parser::Text::currentChar(Input,localPosition);
		localResult=GardenScript::Parser::Parser::getInstance()->internalParse(GardenScript::Parser::Text::currentBody(Input,localPosition,'{','}',1),localInternalResultSize);
	}
	else
		localResult=GardenScript::Parser::Parser::getInstance()->internalParse(Input,localInternalResultSize,1,&localPosition);

	localResult.push_back(Assembler::JMP);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(m_loopBeginOffsets.back()));

	Position+=localPosition;

	return localResult;
}

void Instruction::Compiler::endLoop(bytecode& Loop)
{
	bytecode localEndOfLoopOffset;
	GardenScript::Declaration::Compiler::getInstance()->popScope(Loop);
	
	localEndOfLoopOffset=GardenScript::Compiler::Compiler::getInstance()->adjustConstant(m_breaksOffsets.back()[0]+Loop.size());

	for(unsigned int i=1;i<m_breaksOffsets.back().size();++i)
	{
		if(Loop[m_breaksOffsets.back()[i]-1]!=Assembler::JMP)
			throw Exceptions::InstructionException("Loop[m_breaksOffsets.back()[i]-1]!=Assembler::JMP");

		Loop.replace(Loop.begin()+m_breaksOffsets.back()[i],Loop.begin()+m_breaksOffsets.back()[i]+localEndOfLoopOffset.size(),localEndOfLoopOffset.begin(),localEndOfLoopOffset.end());
	}

	m_breaksOffsets.pop_back();
	m_loopBeginOffsets.pop_back();
}

Instruction::bytecode Instruction::Compiler::compileWhileInstruction(Expression::Expression* Condition,std::string Input,unsigned int& Position)
{
	bytecode localResult;
	bytecode localWorkBody;
	
	initLoop(localResult);
	beginLoop(localResult);
	
	localResult+=GardenScript::Expression::Compiler::getInstance()->compileExpression(Condition);
	localResult.push_back(Assembler::IFA);

	localWorkBody=compileBodyLoop(Input,Position,localResult);	

	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localWorkBody.size()));
	
	localResult+=localWorkBody;

	endLoop(localResult);
	
	return localResult;
}

Instruction::bytecode Instruction::Compiler::compileDoWhileInstruction(Expression::Expression* Condition,std::string Body)
{
	bytecode localResult;
	bytecode localCondition;
	bytecode localWorkBody;
	unsigned localPosition=0;

	const unsigned int localConstantSize=GardenScript::Compiler::Compiler::getInstance()->adjustConstant(0).size();

	initLoop(localResult);
	beginLoop(localResult,localConstantSize+1); //JMP
	
	localCondition=GardenScript::Expression::Compiler::getInstance()->compileExpression(Condition);
	localCondition.push_back(Assembler::IFA);

	localResult.push_back(Assembler::JPD);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localCondition.size()+localConstantSize)); //localConstantSize bo jeszcze 4/8b z IFA

	localResult+=localCondition;
	
	localWorkBody=compileBodyLoop('{'+Body+'}',localPosition,localResult);	

	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localWorkBody.size()));
	
	localResult+=localWorkBody;

	endLoop(localResult);
	
	return localResult;
}

Instruction::bytecode Instruction::Compiler::compileForDeclaration(std::string Declaration)
{
	std::vector<Memory::MemoryObject*> localDeclaration;
	
	unsigned int localPosition=0;

	if(Declaration.empty())
		return "";

	if(GardenScript::TypeParser::Parser::getInstance()->checkIfType(Declaration,0))
		return GardenScript::Declaration::Compiler::getInstance()->compileDeclaration(GardenScript::Declaration::Parser::getInstance()->parseDeclaration(Declaration,localPosition));
	else
		return GardenScript::Expression::Compiler::getInstance()->compileExpression(GardenScript::Expression::Parser::getInstance()->parseExpression(Declaration));
}

Instruction::bytecode Instruction::Compiler::compileForInstruction(std::string Declaration,std::string Condition,std::string Cycle,std::string Input,unsigned int& Position)
{
	bytecode localResult;
	bytecode localWorkBody;
	bytecode localDeclaration;
	bytecode localCycle;
	
	initLoop(localResult);
	
	localResult+=compileForDeclaration(Declaration);

	localCycle=GardenScript::Expression::Compiler::getInstance()->compileExpression(GardenScript::Expression::Parser::getInstance()->parseExpression(Cycle));

	localResult.push_back(Assembler::JPD);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localCycle.size()));

	beginLoop(localResult);

	localResult.append(localCycle);

	if(Condition.size())
	{
		localResult+=GardenScript::Expression::Compiler::getInstance()->compileExpression(GardenScript::Expression::Parser::getInstance()->parseExpression(Condition));

		localResult.push_back(Assembler::IFA);
	}

	localWorkBody=compileBodyLoop(Input,Position,localResult,Condition.size()>0);	

	if(Condition.size())
		localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustJPDConstant(localWorkBody.size()));
	
	localResult+=localWorkBody;

	endLoop(localResult);
	
	return localResult;
}

Instruction::bytecode Instruction::Compiler::compileBreakInstruction()
{
	bytecode localResult;

	if(m_loopBeginOffsets.empty()||m_breaksOffsets.empty())
		throw Exceptions::InstructionException("\"break\" without loop!");

	localResult.push_back(Assembler::JMP);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(0));

	m_breaksOffsets.back().push_back(GardenScript::Parser::Parser::getInstance()->getAllBytecodeSize()+1-m_breaksOffsets.back()[0]);

	return localResult;
}

Instruction::bytecode Instruction::Compiler::compileContinueInstruction()
{
	bytecode localResult;

	if(m_loopBeginOffsets.empty())
		throw Exceptions::InstructionException("\"continue\" without loop!");

	localResult.push_back(Assembler::JMP);
	localResult.append(GardenScript::Compiler::Compiler::getInstance()->adjustConstant(m_loopBeginOffsets.back()));
	
	return localResult;
}

Instruction::bytecode Instruction::Compiler::compilePrintInstruction(Expression::Expression* Expression)
{
	bytecode localResult;
	bool localUnsigned;
	bool localFloat;
	std::vector<GardenScript::TypeParser::ReturnType> localTypes;

	if(!Expression)
		return localResult;

	localResult+=Expression::Compiler::getInstance()->compileExpression(Expression);
	localTypes=Expression::Parser::getInstance()->getTypesOfExpression(Expression);

	//breakpoint
	localFloat=Expression::Parser::getInstance()->isFloat(Expression->calculateReturnType());
	
	if(localFloat)
		localResult.push_back(Assembler::PTF);
	else
	{
		localUnsigned=Expression::Parser::getInstance()->isFlagType(localTypes,TypeParser::F_UNSIGNED);
		localResult.push_back(localUnsigned?Assembler::PTU:Assembler::PTS);
	}
	localResult.push_back(Assembler::RESULT);

	return localResult;
}

Instruction::bytecode Instruction::Compiler::compileReturnInstruction(Expression::Expression* Expression)
{
	GardenScript::Parser::Parser::getInstance()->addWarning("Nie jest sprawdzane potem czy nie przypisuje sie voida");
	bytecode localResult=Expression::Compiler::getInstance()->compileExpression(Expression);

	localResult.push_back(Assembler::RET);

	return localResult;
}