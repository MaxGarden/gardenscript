#include"Declaration.h"
#include"Text.h"
#include"Expression.h"
#include"Function.h"
#include<climits>

using namespace GardenScript::Declaration;

Parser::Parser()
{
}

Parser::~Parser()
{
}

Parser* Parser::getInstance()
{
	static Parser* staticReturn=new Parser();
	
	return staticReturn;
}

GardenScript::Memory::MemoryObject* Parser::parseVariable(std::string Input,unsigned int& Position,GardenScript::Memory::MemoryObject* Object,GardenScript::Memory::Scope* Scope,bool Argument)
{
	std::string localExpression=GardenScript::Parser::Text::currentExpression(Input,Position,',');
	unsigned int localPosition=1;
	std::string localParameter;

	if(localExpression.size())
	{
		if(localExpression[0]=='[')
		{
			parseArray(localExpression,localPosition,Object,Scope,Argument);
			localExpression.erase(localExpression.begin(),localExpression.begin()+localPosition);
		}
		else if(localExpression[0]=='(')
		{
			localParameter=GardenScript::Parser::Text::currentBody(localExpression,localPosition,'(',')',1);
			
			if(localParameter.empty()||GardenScript::TypeParser::Parser::getInstance()->checkIfType(localParameter,0))
				Object=parseFunction(localExpression,localPosition,Object,localParameter,Scope,Argument);
			else
			{
				throw Exceptions::DeclarationException("U�yto (), kt�re jest jeszcze nie zaimplementowane dla zmiennych!");
			}

			localExpression.erase(localExpression.begin(),localExpression.begin()+localPosition);
		}
	}

	if(localExpression.size()&&localExpression.front()!='='&&localExpression.front())
		throw Exceptions::DeclarationException("Did you forget \";\"?");
	else
	{
		if(Object->m_type.m_flags&GardenScript::TypeParser::T_REFERENCE)
		{
			if(localExpression.empty())
				throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("\"%s\" references must be initialized!",Object->m_type.m_name.c_str()));

			localExpression.erase(localExpression.begin());

			Object->m_defaultValue=Expression::Parser::getInstance()->parseExpression(localExpression);

			if(Object->m_defaultValue->m_type!=Expression::Expression::IDENTIFIER||((Expression::IdentifierOperator*)Object->m_defaultValue)->getExpression())
				throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("\"%s\" reference must be initialized by identifier!",Object->m_type.m_name.c_str()));

			Object->m_type.m_flags^=TypeParser::T_REFERENCE;

			if(!TypeParser::ReturnType::compare(&Object->m_type,&((Expression::IdentifierOperator*)Object->m_defaultValue)->getObject()->m_type))
				throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("\"%s\" reference initialization types aren't same!",Object->m_type.m_name.c_str()));
		
			Object->m_type.m_flags^=TypeParser::T_REFERENCE;
		}
		else if(localExpression.size())
		{
			localExpression.insert(localExpression.begin(),Object->m_type.m_name.begin(),Object->m_type.m_name.end());
			Object->m_defaultValue=Expression::Parser::getInstance()->parseExpression(localExpression);
		}
	}

	if(Object->m_type.m_arrayDimension.size()&&Object->m_type.m_arrayDimension[0]==UINT_MAX)
		assignArraySize(Object);

	if(GardenScript::Parser::Text::currentChar(Input,Position,false)==',')
		++Position;

	return Object;
}

void Parser::assignArraySize(GardenScript::Memory::MemoryObject* Array)
{
	Expression::BinaryOperator* localDefaultValue;
	Expression::StaticDataOperator* localStaticDataValue;

	localDefaultValue=dynamic_cast<Expression::BinaryOperator*>(Array->m_defaultValue);

	if(!localDefaultValue)
		throw Exceptions::DeclarationException("!localDefaultValue");

	localStaticDataValue=dynamic_cast<Expression::StaticDataOperator*>(localDefaultValue->m_right);

	if(!localStaticDataValue)
		throw Exceptions::DeclarationException("!localStaticDataValue");

	Array->m_type.m_arrayDimension[0]=localStaticDataValue->getSize(1);
}

GardenScript::Memory::MemoryObject* Parser::parseArray(std::string Input,unsigned int& Position,GardenScript::Memory::MemoryObject* Object,GardenScript::Memory::Scope* Scope,bool Argument)
{
	__int64 localIntParameter=0;
	std::string localParameter;
	Expression::Expression* localConstantExpression;

	while(true)
	{
		localParameter=GardenScript::Parser::Text::currentBody(Input,Position,'[',']',1);

		if(localParameter.size())
		{
			localConstantExpression=GardenScript::Expression::Parser::getInstance()->parseExpression(localParameter);

			if(!localConstantExpression)
				throw Exceptions::DeclarationException("Invalid size of array!");

			localIntParameter=GardenScript::Expression::Parser::getInstance()->calculateConstantExpression(localConstantExpression);
			
			if(localIntParameter<=0)
				throw Exceptions::DeclarationException("The size of array dimension must be greater than zero!");
			else if(localIntParameter>=UINT_MAX)
				throw Exceptions::DeclarationException("The size of array dimension must be less than UINT_MAX!");
		}
		else
		{
			if(Object->m_type.m_arrayDimension.size())
				throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("\"%s\" missing subscript!",Object->m_type.m_name.c_str()));
				
			localIntParameter=UINT_MAX;
		}

		if(Object->m_type.m_arrayDimension.size()&&Object->m_type.m_arrayDimension.back()==UINT_MAX)
			throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("\"%s\" unknown size!",Object->m_type.m_name.c_str()));

		Object->m_type.m_arrayDimension.push_back((unsigned int)localIntParameter);
		//localExpression.erase(localExpression.begin(),localExpression.begin()+localPosition);

		if(Position+1<Input.size()&&Input[Position]=='[')
			++Position;
		else
			break;
	}

	//if(Object->m_type.m_arrayDimension.back()==UINT_MAX)
	//	throw Exceptions::DeclarationException("NIE ZROBIONE< TRZEBA ZROBIC");

	return 0;
}

unsigned int Parser::parseArguments(std::string Input,GardenScript::Memory::Function* Function)
{
	unsigned int localPosition=0;
	unsigned int localCurrentVariant;
	unsigned int localZeroPosition=0;
	bool localDefaultArgument=false;
	Memory::MemoryObject* localObject;

	std::vector<Memory::MemoryObject*> localBuffer;

	Memory::Scope* localScope=new Memory::Scope();
	
	std::vector<TypeParser::ReturnType*> localArgumentsReturn;

	Memory::MemoryManager::getInstance()->pushScope(localScope);

	while(GardenScript::Parser::Text::currentChar(Input,localPosition,false)!='\0')
	{
		localBuffer=parseDeclaration(GardenScript::Parser::Text::currentArgument(Input,localPosition,"),"),localZeroPosition,localScope,true);

		if(localBuffer.empty())
			throw Exceptions::DeclarationException("PARSER ERROR P02D: BUFFER IS EMPTY!");

		localObject=localBuffer[0];
		localArgumentsReturn.push_back(localObject->m_type.copy());

		if(localObject->m_defaultValue)
			localDefaultArgument=true;

		localZeroPosition=0;
	}

	Memory::MemoryManager::getInstance()->popScope();

	localCurrentVariant=Function->getIndexVariant(localArgumentsReturn,localScope); 

	if(localCurrentVariant==Function->getVariantsSize()+1)
	{
		localCurrentVariant=Function->getVariantsSize();
		Function->addVariant();

		for(unsigned int i=0;i<localArgumentsReturn.size();++i)
			Function->addArgument(localCurrentVariant,localArgumentsReturn[i]);

		Function->setScope(localCurrentVariant,localScope);
	}
	else
	{
		if(localDefaultArgument)
			throw Exceptions::DeclarationException("Arguments with default values are allowed only with first declaration!");
	}

	return localCurrentVariant;
	
}

GardenScript::Memory::MemoryObject* Parser::parseFunction(std::string Input,unsigned int& Position,GardenScript::Memory::MemoryObject* Object,std::string Parameters,GardenScript::Memory::Scope* Scope,bool Argument)
{
	std::string localBody;
	unsigned int localVariant;	
	Memory::Function* localReturn;
	
	if(Object->m_multipleDefine==false)
	{
		localReturn=new Memory::Function();
		localReturn->m_type=*Object->m_type.copy();
		localReturn->m_type.m_name=Object->m_type.m_name;
		localReturn->m_identyfier=Object->m_identyfier;
		localReturn->m_multipleDefine=true;
		
		Scope->setObject(Object->m_type.m_name,localReturn);
	}
	else
		localReturn=(Memory::Function*)Object;
	
	localVariant=parseArguments(Parameters,localReturn);

	if(GardenScript::Parser::Text::currentChar(Input,Position)=='{')
	{
		localBody=GardenScript::Parser::Text::currentBody(Input,Position,'{','}',1);
		
		localReturn->setBody(localVariant,localBody);
		//throw Exceptions::DeclarationException("TERAZ DEFINICJA!");
	}

	//throw Exceptions::DeclarationException("TERAZ DEFINICJA!");
	
	return localReturn;
}

std::vector<GardenScript::Memory::MemoryObject*> Parser::parse(std::string Input,unsigned int& Position,GardenScript::Memory::Scope* Scope,bool Argument)
{
	static unsigned int staticArgumentNumber=0;

	std::vector<GardenScript::Memory::MemoryObject*> localReturn;
	
	TypeParser::ReturnType* localType=GardenScript::TypeParser::Parser::getInstance()->parseType(Input,Position);

	std::string localName;
	std::string localExpression;
	std::string localParameter;

	
	if(!localType)
		return localReturn;

	if(Argument&&GardenScript::Parser::Text::currentChar(Input,Position,false)=='\0')
	{
		localReturn.push_back(new Memory::MemoryObject);

		localReturn.back()->m_type=*localType->copy();
		localReturn.back()->m_type.m_name=GardenScript::Parser::Text::stringFormat("@argument%d@",staticArgumentNumber++);
		localReturn.back()->m_identyfier=0;

		try
		{
			Scope->addObject(localReturn.back());
		}
		catch(Exceptions::Exception& e)
		{
			if(!Scope->getObject(localName)->m_multipleDefine)
				throw e;

			delete localReturn.back();
			localReturn.back()=Scope->getObject(localName);
		}

	}
	//if variable{
	while(GardenScript::Parser::Text::currentChar(Input,Position,false)!='\0')
	{
		localName=GardenScript::Parser::Text::currentName(Input,Position);

		if(localName.empty())
			throw Exceptions::DeclarationException(GardenScript::Parser::Text::stringFormat("Bad name format \"%s\"!",GardenScript::Parser::Text::currentString(Input,Position,false).c_str()));

		localReturn.push_back(new Memory::MemoryObject);
	
		localReturn.back()->m_type=*localType->copy();

		localReturn.back()->m_type.m_name=localName;

		localReturn.back()->m_identyfier=0;//new Bytecode::Identifier(localType->m_type.m_size);

		try
		{
			Scope->addObject(localReturn.back());
		}
		catch(Exceptions::Exception& e)
		{
			if(!Scope->getObject(localName)->m_multipleDefine)
				throw e;

			delete localReturn.back();
			localReturn.back()=Scope->getObject(localName);
		}

		localReturn.back()=parseVariable(Input,Position,localReturn.back(),Scope,Argument);
	}
	//}

	delete localType;

	return localReturn;
}


std::vector<GardenScript::Memory::MemoryObject*> Parser::parseDeclaration(std::string Input,unsigned int& Position,bool Arguments)
{
	std::vector<GardenScript::Memory::MemoryObject*> localReturn;

	try
	{
		localReturn=parse(Input,Position,GardenScript::Memory::MemoryManager::getInstance()->getCurrentScope(),Arguments);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);
	}

	return localReturn;
}

std::vector<GardenScript::Memory::MemoryObject*> Parser::parseDeclaration(std::string Input,unsigned int& Position,GardenScript::Memory::Scope* Scope,bool Arguments)
{
	std::vector<GardenScript::Memory::MemoryObject*> localReturn;

	try
	{
		localReturn=parse(Input,Position,Scope,Arguments);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);
	}

	return localReturn;
}