#pragma once
#include"Expression.h"
#include"VirtualMemory.h"
#include<map>

namespace GardenScript
{
	namespace Expression
	{
		typedef std::string bytecode;

		class Compiler
		{
		private:
			Compiler();
			~Compiler();
		private:
			bool isFloat(Expression* expression);
		protected:
			unsigned char m_lastRegister;
			std::vector<Bytecode::Identifier*> m_updateIndentyfiers;
			std::vector<unsigned char> m_updateIdentifiersRegisters;
			Expression::TYPE m_expressionType;
			std::map<Bytecode::Identifier*,unsigned char> m_identyfiers;
			std::map<unsigned char,unsigned char> m_dereferencedIdentifiers;
		private:
			bytecode constantExpression(ConstantOperator* expression);
			bytecode binaryExpression(BinaryOperator* expression);
			bytecode _binaryExpression(BinaryOperator* expression);
			bytecode _pointerExpression(BinaryOperator* expression);
			bytecode _dereferenceExpression(DereferenceOperator* expression);
			bytecode dereferenceExpression(DereferenceOperator* expression);
			bytecode identyfierExpression(IdentifierOperator* expression);

			bytecode typecast(IdentifierOperator* expression);

			bytecode compile(Expression* expression);
		public:
			static Compiler* getInstance();

			bytecode compileExpression(Expression* expression);
		};
	}
}