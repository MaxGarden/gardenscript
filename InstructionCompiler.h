#pragma once
#include"Compiler.h"
#include"Instruction.h"
#include"Expression.h"

namespace GardenScript
{
	namespace Instruction
	{
		typedef std::string bytecode;

		class Compiler
		{
		private:
			Compiler();
			~Compiler();
		protected:
			std::vector<unsigned int> m_loopBeginOffsets;
			std::vector<std::vector<unsigned int>> m_breaksOffsets;
		private:
			void initLoop(bytecode& Loop);
			void beginLoop(bytecode& Loop,unsigned int Offset=0);
			bytecode compileBodyLoop(std::string Input,unsigned int& Position,bytecode Begin,bool Condition=true);
			void endLoop(bytecode& Loop);

			bytecode compileForDeclaration(std::string Declaration);
		public:
			static Compiler* getInstance();

			bytecode compileIfInstruction(Expression::Expression* Condition,std::string Input,unsigned int& Position,std::string IfBody);
			bytecode compileWhileInstruction(Expression::Expression* Condition,std::string Input,unsigned int& Position);
			bytecode compileDoWhileInstruction(Expression::Expression* Condition,std::string Body);
			bytecode compileForInstruction(std::string Declaration,std::string Condition,std::string Cycle,std::string Input,unsigned int& Position);
			bytecode compileBreakInstruction();
			bytecode compileContinueInstruction();

			bytecode compilePrintInstruction(Expression::Expression* Expression);
			bytecode compileReturnInstruction(Expression::Expression* Expression);
		};
	}
}