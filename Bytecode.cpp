#include"Bytecode.h"
#include"Memory.h"

using namespace GardenScript::Bytecode;

Identifier::Identifier(unsigned int Address,unsigned char Size)
{
	throw Exceptions::IdentifierException("1234");
	m_address=Address;
	m_size=Size;
	m_scope=Memory::MemoryManager::getInstance()->getCurrentScopeIndex();
	m_stackAddress=0;
}

Identifier::Identifier(unsigned char Size,bool ReserveAddress)
{
	m_size=Size;
	m_address=GardenScript::Memory::MemoryManager::getInstance()->getCurrentScope()->getNextAddress(Size,ReserveAddress);
	m_scope=Memory::MemoryManager::getInstance()->getCurrentScopeIndex();
	m_stackAddress=0;
}

Identifier::~Identifier()
{
}

unsigned _int64 Identifier::getAddress()
{
	return m_address;
}

unsigned char Identifier::getSize()
{
	return m_size;
}

Constant::Constant(void* Value,unsigned char Size)
{
	m_value=Value;
	m_size=Size; 
}

Constant::~Constant()
{
}

void* Constant::getValue()
{
	return m_value;
}

unsigned char Constant::getSize()
{
	return m_size;
}