#pragma once
#include<Assembler.h>
#include<string>

namespace GardenScript
{
	namespace Compiler
	{
		typedef std::string bytecode;

		class Compiler
		{
		public:
			enum TARGET_TYPE
			{
				_32b=4,
				_64b=8
			};
		private:
			Compiler();
			~Compiler();
		protected:
			bool m_isSetType;
			TARGET_TYPE m_machineType;
			unsigned int m_registersIndex;
		public:
			bool init(const char* Flags,TARGET_TYPE TargerType=_64b);

			bytecode compile(std::string Input,unsigned int& Position);

			void setRegisterValue(unsigned char Register,__int64 Value);

			std::string adjustConstant(__int64 Constant); 
			std::string adjustConstant(__int64 Constant,unsigned char Size);

			std::string adjustJPDConstant(__int64 Constant);

			TARGET_TYPE getMachineType();

			unsigned char findRegister(__int64 Constant);

			unsigned char nextRegister();
			unsigned char freeRegister(__int64 Constant=0);

			static Compiler* getInstance();
		};
	}
}