#pragma once
#include"Text.h"
#include"Parser.h"
#include"Bytecode.h"

namespace GardenScript
{
	namespace Expression
	{
		class IdentifierOperator;

		class IdentifierParser
		{
		private:
			IdentifierParser();
			~IdentifierParser();
		private:
		public:
			static IdentifierParser* getInstance();

			IdentifierOperator* parse(std::string Input,unsigned int& Position); 
		};
	}
}
