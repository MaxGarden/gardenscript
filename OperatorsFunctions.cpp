#include"Expression.h"
#include"Text.h"
#include"Memory.h"
#include"Type.h"
#include"Compiler.h"
#include"Constant.h"
#include"Function.h"

using namespace GardenScript;
//using namespace GardenScript::Expression;

Expression::ConditionalOperator::ConditionalOperator(Expression* Condition,Expression* FirstExpression,Expression* SecondExpression)
{
	m_condition=Condition;
	m_firstExpression=FirstExpression;
	m_secondExpression=SecondExpression;
}

Expression::ConditionalOperator::~ConditionalOperator()
{
	delete m_condition;
	delete m_firstExpression;
	delete m_secondExpression;
}

Expression::Expression* Expression::ConditionalOperator::operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{

	throw Exceptions::OperatorException("Nie zrobione");
	ConditionalOperator* localResult=new ConditionalOperator(Identifier,0,0);
	localResult->m_firstExpression=Parser::getInstance()->parseExpression(GardenScript::Parser::Text::currentBody(Input,Position,'?',':',1));
	localResult->m_secondExpression=Parser::getInstance()->parseExpression(GardenScript::Parser::Text::cutString(Input,Position));

	return localResult;
}

GardenScript::TypeParser::ReturnType Expression::ConditionalOperator::calculateReturnType()
{
	return m_firstExpression->calculateReturnType();
}

Expression::Expression* Expression::BinaryOperator::bracketOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	if(!Identifier)
		return TypecastOperator::operatorFunction(Identifier,Input,Position);
	else
		return CallOperator::operatorFunction(Identifier,Input,Position);
}

Expression::Expression* Expression::BinaryOperator::arrayOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	Expression* localResult=0;
	unsigned int localPosition=Position;
	std::string localBody=GardenScript::Parser::Text::currentBody(Input,Position,'[',']',1);
	IdentifierOperator* localIdentifier=dynamic_cast<IdentifierOperator*>(Identifier);

	if(!Identifier)
		return 0;

	if(!localIdentifier)
	{
		if(Identifier->m_type==BINARY)
		{
			localResult=arrayOperatorFunction(((BinaryOperator*)Identifier)->m_left,Input,localPosition);

			Position=localPosition;

			if(localResult)
				((BinaryOperator*)Identifier)->m_left=localResult;
		}

		if(!localResult)
			throw Exceptions::OperatorException("Identifier is not identifier!");

		return Identifier;
	}

	return Parser::getInstance()->parseExpression(GardenScript::Parser::Text::stringFormat("*(%s+%s)",localIdentifier->getObject()->m_type.m_name.c_str(),localBody.c_str()));
}

Expression::Expression* Expression::ConstantOperator::unaryMinusOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	Expression* localExpression;
	long long int localBuffer;

	localExpression=Parser::getInstance()->identifier(Input,Position);

	if(!Identifier&&localExpression->m_type==CONSTANT)
	{
		if(((ConstantOperator*)localExpression)->m_size>8)
			throw Exceptions::OperatorException("Size of constant value is bigger than 64b!");

		memcpy(&localBuffer,((ConstantOperator*)localExpression)->m_value,((ConstantOperator*)localExpression)->m_size);
		localBuffer=-localBuffer;
		memcpy(((ConstantOperator*)localExpression)->m_value,&localBuffer,((ConstantOperator*)localExpression)->m_size);
	
		return localExpression;
	}

	return new BinaryOperator(BinaryOperator::s_unaryMinusOperator,localExpression,0);
}

Expression::Expression* Expression::ConstantOperator::sizeofOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	unsigned int localPosition=0;
	std::string localTypeName=GardenScript::Parser::Text::currentBody(Input,++Position,'(',')',1);
	std::string localName;
	unsigned __int64 localSize;

	GardenScript::Parser::Parser::getInstance()->addWarning("Trzeba zrobi� �eby nie by�o wymagane () oraz, �eby by�a obs�uga wyra�e� $(*x)");

	if(TypeParser::Parser::getInstance()->checkIfType(localTypeName,0))
	{
		localSize=TypeParser::Parser::getInstance()->parseType(localTypeName,localPosition)->m_type.m_size;
		return new ConstantOperator(&localSize,sizeof(localSize),TypeParser::ReturnType::intType());
	}

	localName=GardenScript::Parser::Text::currentString(localTypeName,localPosition);

	if(GardenScript::Parser::Text::currentChar(localTypeName,localPosition)!='\0')
		throw Exceptions::OperatorException(GardenScript::Parser::Text::stringFormat("Undefined type \"%s\"!",localTypeName.c_str()));
	
	if(Memory::MemoryManager::getInstance()->isObject(localName))
	{
		localSize=Memory::MemoryManager::getInstance()->getObject(localName)->m_type.getStackSize();
		return new ConstantOperator(&localSize,sizeof(localSize),TypeParser::ReturnType::intType());
	}
	else
		throw Exceptions::OperatorException(GardenScript::Parser::Text::stringFormat("\"%s\" is not type or identifier!",localName.c_str()));

	return 0;
}

Expression::TypecastOperator::TypecastOperator(TypeParser::ReturnType* Type)
{
	m_type=Type;
}

Expression::TypecastOperator::~TypecastOperator()
{
	//nie usuwa� type
}

GardenScript::TypeParser::ReturnType Expression::TypecastOperator::calculateReturnType()
{
	return *m_type;
}

Expression::Expression* Expression::TypecastOperator::operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	Expression* localResult;
	TypeParser::ReturnType* localType;
	Memory::MemoryObject* localMemoryObject;
	Expression* localExpression;
	unsigned int localPosition=0;
	
	std::string localBracket=GardenScript::Parser::Text::currentBody(Input,Position,'(',')',1);

	if(TypeParser::Parser::getInstance()->checkIfType(localBracket,0))
	{
		localType=TypeParser::Parser::getInstance()->parseType(localBracket,localPosition);

		if(!localType->m_type.m_pod)
			throw Exceptions::OperatorException("Cannot cast non-pod!");

		localExpression=Parser::getInstance()->identifier(Input,Position);

		if(!localExpression)
			throw Exceptions::OperatorException("Syntax error \")\"!");

		if(GardenScript::Parser::Text::currentChar(localBracket,localPosition)!='\0')
			throw Exceptions::OperatorException(GardenScript::Parser::Text::stringFormat("Undefined something \"%s\" after type!",GardenScript::Parser::Text::cutString(localBracket,localPosition-1).c_str()));
		

		localMemoryObject=new Memory::MemoryObject;

		memset(localMemoryObject,0,sizeof(Memory::MemoryObject));

		localMemoryObject->m_type=*localType;
		localMemoryObject->m_identyfier=new Bytecode::Identifier(0);

		localResult=new IdentifierOperator(localMemoryObject,localExpression);

		/*
		if(localResult->m_type==Expression::BINARY)
			localExpression=((BinaryOperator*)localExpression)->m_left,throw Exceptions::OperatorException("Jak b�dzie 1+1+1 to nie zadzia�a");
	
		if(localExpression->m_type==Expression::IDENTIFIER)
			((IdentifierOperator*)localExpression)->getObject()->m_type=*localType;
		else if(localExpression->m_type==Expression::DEREFERENCE)
			((DereferenceOperator*)localExpression)->getObject()->m_type=*localType;
		else
			throw Exceptions::OperatorException("nie zrobione!");
			*/
		delete localType;
		return localResult;
	}

	return Parser::getInstance()->parseExpression(localBracket);
}

Expression::DereferenceOperator::DereferenceOperator(Expression* expression,IdentifierOperator* Pointer,unsigned char Operator):IdentifierOperator(*Pointer)
{
	m_type=DEREFERENCE;

	m_dereferenceExpression=expression;

	if(Operator&&(Operator!=BinaryOperator::s_additionOperator&&Operator!=BinaryOperator::s_subtractionOperator)) 
		throw Exceptions::OperatorException("Invalid operator used with a pointer!");
	
	m_operator=Operator?BinaryOperator::s_additionOperator:0;
}


Expression::DereferenceOperator::~DereferenceOperator()
{
	delete m_dereferenceExpression;
}

Expression::Expression* Expression::DereferenceOperator::operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	static unsigned int staticRecursionLevel=0;

	++staticRecursionLevel;
	IdentifierOperator* localPointer;
	Memory::MemoryObject* localMemoryObject;

	Expression* localExpression=GardenScript::Expression::Parser::getInstance()->identifier(Input,Position);

	unsigned char localOperator=0;

	if(!localExpression)
	{
		staticRecursionLevel=0;
		return 0;
	}

	if(localExpression->m_type==Expression::IDENTIFIER)
	{
		if(!((IdentifierOperator*)localExpression)->getObject()->m_type.m_pointer&&!((IdentifierOperator*)localExpression)->getObject()->m_type.m_arrayDimension.size())
		{
			staticRecursionLevel=0;
			throw Exceptions::OperatorException("Identifier must be a pointer or an array!");
		}

		localPointer=(IdentifierOperator*)localExpression;

	}
	else if(localExpression->m_type==Expression::BINARY)
	{
		localPointer=findPointerOrArray((BinaryOperator*)localExpression,localOperator,&localExpression);

		if(!localPointer)
		{
			staticRecursionLevel=0;
			throw Exceptions::OperatorException("Illegal indirection: operant must contain a pointer or an array!");
		}
	}
	else if(localExpression->m_type==Expression::DEREFERENCE)
	{
		if(((DereferenceOperator*)localExpression)->getObject()->m_type.m_pointer<=staticRecursionLevel)
		{
			staticRecursionLevel=0;
			throw Exceptions::OperatorException("Illegal indirection: operant must be a pointer or an array!"); 
		}

		localPointer=(IdentifierOperator*)localExpression;
	}
	else
	{
		staticRecursionLevel=0;
		throw Exceptions::OperatorException("Illegal indirection: operant must be a pointer or an array!"); 
	}

	--staticRecursionLevel;

	localMemoryObject=new Memory::MemoryObject(*localPointer->getObject());

	if(localMemoryObject->m_type.m_arrayDimension.empty())
		--localMemoryObject->m_type.m_pointer;


	return new IdentifierOperator(localMemoryObject,new DereferenceOperator(localExpression,localPointer,localOperator));
}

Expression::IdentifierOperator* Expression::DereferenceOperator::findPointerOrArray(BinaryOperator* Binary,unsigned char& Operator,Expression** Parent)
{
	IdentifierOperator* localResult;
	static IdentifierOperator* staticReturn=0;
	static BinaryOperator* staticOperator=0;

	unsigned __int64 localZero=0;

	if(staticOperator==0)
	{
		staticOperator=Binary;
		staticReturn=0;
	}

	std::vector<Expression**> localExpressions;
	localExpressions.push_back(&Binary->m_left);
	localExpressions.push_back(&Binary->m_right);

	for(unsigned int i=0;i<localExpressions.size();++i)
	{
		if(*localExpressions[i])
		{
			if((*localExpressions[i])->m_type==Expression::BINARY)
				findPointerOrArray((BinaryOperator*)*localExpressions[i],Operator,localExpressions[i]);
			else if((*localExpressions[i])->m_type==Expression::IDENTIFIER)
			{
				localResult=(IdentifierOperator*)*localExpressions[i];
				
				Operator=Binary->getOperator();

				if(localResult->getObject()->m_type.m_pointer||localResult->getObject()->m_type.m_arrayDimension.size())
				{
					GardenScript::Parser::Parser::getInstance()->addWarning("Trzeba sprawdzic czy nie jest dereferencjonowany albo wywo�ywany np *(x-tab[0]) a potem czy nie ma zakazanych operator�w np *(-x)","operator function parser");	
			
					if(Operator==BinaryOperator::s_subtractionOperator&&i==1)//sytuacja *(1-ptr)
						throw Exceptions::OperatorException("Cannot sub a pointer!");

					if(!staticReturn)
					{
						if((Parser::getInstance()->getOperatorString(Binary->getOperator())!="-"&&Parser::getInstance()->getOperatorString(Binary->getOperator())!="+")||Parser::getInstance()->getOperatorAssociativity(Binary->getOperator())&Parser::BEGIN)
						{
							//trzeba sprawdzi� czy nie ma dereferencji "*x"
							staticOperator=0;
							throw Exceptions::OperatorException("Not allowed operation on pointer!");
						}

						staticReturn=localResult;

						//*Parent=*localExpressions[i?0:1];
						*localExpressions[i]=new ConstantOperator(&localZero,GardenScript::Compiler::Compiler::getInstance()->getMachineType(),TypeParser::ReturnType::intType());
					}
					else
					{
						staticOperator=0;
						throw Exceptions::OperatorException("Dereference with two pointers!");
					}
				}
			}
		}
	}


	if(Binary==staticOperator)
	{
		staticOperator=0;
		
		localResult=staticReturn;
		staticReturn=0;
	}

	return localResult;
}

Expression::Expression* Expression::DereferenceOperator::getDereferenceExpression()
{
	return m_dereferenceExpression;
}

unsigned char Expression::DereferenceOperator::getOperator()
{
	return m_operator;
}

GardenScript::TypeParser::ReturnType Expression::DereferenceOperator::calculateReturnType()
{
	TypeParser::ReturnType localResult;
	
	if(m_dereferenceExpression->m_type==IDENTIFIER)
		localResult=m_dereferenceExpression->calculateReturnType();
	else
		localResult=m_object->m_type;

	--localResult.m_pointer;

	return localResult;
}

Expression::IdentifierOperator::IdentifierOperator(Memory::MemoryObject* Object,Expression* expression)
{
	m_object=Object;
	m_expression=expression;
	
	m_type=IDENTIFIER;
}


Expression::IdentifierOperator::~IdentifierOperator()
{
	delete m_object;
	delete m_expression;
}

Memory::MemoryObject* Expression::IdentifierOperator::getObject()
{
	return m_object;
}

Expression::Expression* Expression::IdentifierOperator::getExpression()
{
	return m_expression;
}


GardenScript::TypeParser::ReturnType Expression::IdentifierOperator::calculateReturnType()
{
	if((!m_object->m_identyfier||m_object->m_identyfier->m_size)&&getExpression())
		return getExpression()->calculateReturnType();
	
	return m_object->m_type;
}

Expression::Expression* Expression::IdentifierOperator::addressOperatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	Expression* localIdentifier=Parser::getInstance()->identifier(Input,Position);
	Memory::MemoryObject* localMemoryObject;

	BinaryOperator* localBinaryOperator;

	if(!localIdentifier)
		return 0;

	if(localIdentifier->m_type!=IDENTIFIER)
		throw Exceptions::OperatorException("Address of non identifier is not allowed!");

	localBinaryOperator=dynamic_cast<BinaryOperator*>(((IdentifierOperator*)localIdentifier)->getExpression());

	if(localBinaryOperator&&localBinaryOperator->getOperator()==BinaryOperator::s_addressOperator)
		throw Exceptions::OperatorException("Cannot get an address of an address!");
	
	localMemoryObject=new Memory::MemoryObject(*((IdentifierOperator*)localIdentifier)->getObject());
	++localMemoryObject->m_type.m_pointer;

	return new IdentifierOperator(localMemoryObject,new BinaryOperator(BinaryOperator::s_addressOperator,localIdentifier,0));
}

Expression::StaticDataOperator::StaticDataOperator(void* PointerToData,unsigned int Size,std::vector<unsigned int> ElementsSize)
{
	m_data=PointerToData;
	m_size=Size;
	m_elementsSize=ElementsSize;

	m_type=STATIC_DATA;
}

Expression::StaticDataOperator::~StaticDataOperator()
{
	delete m_data;
}

GardenScript::TypeParser::ReturnType Expression::StaticDataOperator::calculateReturnType()
{
	return TypeParser::ReturnType::charType();
}

Expression::Expression* Expression::StaticDataOperator::operatorFunction(Expression* Identifier,std::string Input,unsigned int& Position)
{
	std::vector<__int64> localValues;
	std::vector<unsigned int> localElementsSize;

	void* localData;
	unsigned int localSize=0;
	Expression* localConstantExpression;
	unsigned int localPosition=0;
	std::string localDataText=GardenScript::Parser::Text::currentBody(Input,Position,'{','}',1,true);


	while(localPosition<localDataText.size())
	{
		localConstantExpression=Parser::getInstance()->parseExpression(GardenScript::Parser::Text::currentExpression(localDataText,localPosition,','));
		GardenScript::Parser::Text::currentChar(localDataText,localPosition);

		if(!localConstantExpression)
			throw Exceptions::OperatorException("Invalid static data!");

		localElementsSize.push_back(0);

		localValues.push_back(Parser::getInstance()->calculateConstantExpression(localConstantExpression,&localElementsSize.back()));
	
		localSize+=localElementsSize.back();
	}

	localData=new char[localSize];

	localPosition=0;

	for(unsigned int i=0;i<localValues.size();localPosition+=localElementsSize[i++])
		memcpy(&((char*)localData)[localPosition],&localValues[i],localElementsSize[i]);


	return new StaticDataOperator(localData,localSize,localElementsSize);
}

void* Expression::StaticDataOperator::createAdjutedData(unsigned int ElementSize,unsigned int& DataSize)
{
	DataSize=m_elementsSize.size()*ElementSize;

	char* localReturn=new char[DataSize];
	unsigned __int64 localPosition=0;

	for(unsigned int i=0;i<m_elementsSize.size();++i)
	{
		memcpy(&localReturn[ElementSize*i],&((char*)m_data)[localPosition],ElementSize);
		localPosition+=m_elementsSize[i];
	}

	return localReturn;
}

unsigned int Expression::StaticDataOperator::getSize(unsigned int ElementSize)
{
	if(ElementSize==0)
		return (unsigned int)m_size;
	
	return m_elementsSize.size()*ElementSize;
}

Expression::CallOperator::CallOperator()
{
}

Expression::CallOperator::~CallOperator()
{
}

GardenScript::TypeParser::ReturnType Expression::CallOperator::calculateReturnType()
{
	throw Exceptions::ExpressionException("nie zrobione");
	return TypeParser::ReturnType::charType();
}

std::string Expression::CallOperator::argumentsReturnTypeNamesToString(std::vector<TypeParser::ReturnType*> Arguments)
{
	std::string localResult="(";

	for(unsigned int i=0;i<Arguments.size();++i)
	{
		localResult.append(Arguments[i]->getReturnTypeName());
		localResult.push_back(',');
	}

	*(localResult.end()-1)=')';

	return localResult;
}

Expression::Expression* Expression::CallOperator::operatorFunction(Expression* Identyfier,std::string Input,unsigned int& Position)
{
	unsigned int localVariant;
	std::string localArguments;
	Memory::MemoryObject* localObject;
	Memory::Function* localFunction;
	std::vector<Expression*> localParsedArguments;
	std::vector<TypeParser::ReturnType> localArgumentsBuffer;
	std::vector<TypeParser::ReturnType*> localArgumentsTypes;

	if(Identyfier->m_type!=Expression::IDENTIFIER)
		throw Exceptions::OperatorException("Term does not evaluate to a function!");
	
	localObject=((IdentifierOperator*)Identyfier)->getObject();

	if(!localObject->m_multipleDefine)
		throw Exceptions::OperatorException("Identifier is not a function!");

	localFunction=(Memory::Function*)localObject;

	localArguments=GardenScript::Parser::Text::currentBody(Input,Position,'(',')',1,true);

	localParsedArguments=parseArguments(localArguments);

	localArgumentsBuffer.resize(localParsedArguments.size());

	for(unsigned int i=0;i<localParsedArguments.size();++i)
	{
		localArgumentsBuffer[i]=localParsedArguments[i]->calculateReturnType();
		localArgumentsTypes.push_back(&localArgumentsBuffer[i]);
	}

	localVariant=localFunction->getIndexVariant(localArgumentsTypes,0);

	if(localVariant>localFunction->getVariantsSize())
		throw Exceptions::OperatorException(GardenScript::Parser::Text::stringFormat("There is no a variant \"%s\" of function \"%s\"",argumentsReturnTypeNamesToString(localArgumentsTypes).c_str(),localFunction->m_type.m_name.c_str()));
	

	throw Exceptions::OperatorException("TUTAJ SKONCZYLEM");

	return new CallOperator();
}

std::vector<Expression::Expression*> Expression::CallOperator::parseArguments(std::string Input)
{
	std::vector<Expression*> localResult;

	unsigned int localPosition=0;
	std::string localCurrentExpression;

	while(true)
	{
		localCurrentExpression=GardenScript::Parser::Text::currentExpression(Input,localPosition,',',true);
		++localPosition;

		if(localCurrentExpression.empty())
			break;

		localResult.push_back(Parser::getInstance()->parseExpression(localCurrentExpression));
	}

	return localResult;
}