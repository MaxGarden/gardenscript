#pragma once
#include<vector>
#include<string>
#include"Exceptions.h"

namespace GardenScript
{
	namespace LexicalParser
	{
		struct Lexem;
	}

	namespace Parser
	{
		struct Line
		{
			Line(std::string Source,LexicalParser::Lexem* Lexem,unsigned int LineNumber);
			~Line();

			std::string m_source;
			LexicalParser::Lexem* m_lexem;

			unsigned int m_lineNumber;
		};

		class Parser
		{
		protected:
			std::vector<std::string> m_lines;
			std::vector<std::string> m_log;

			unsigned int m_beginLineNumber;
			unsigned int m_currentLineNumber;
			bool m_error;
			unsigned int m_bytecodeSize;
			unsigned int m_internalBytecodeSize;
		private:
			Parser();
			~Parser();

			std::string _parse(std::string Input,unsigned int& Position);
		public:
			std::vector<std::string> m_stringOperators;

			void addError(Exceptions::Exception Error);
			void addWarning(std::string Warning,std::string Type="");

			bool isError();

			std::string internalParse(std::string Input,unsigned int Offset,unsigned int Lines=-1,unsigned int* Position=0);
			std::string parse(std::string Input,unsigned int& Position,unsigned int Offset=0);

			void showLog();

			unsigned int getBytecodeSize();
			unsigned int getInternalBytecodeSize();
			unsigned int getAllBytecodeSize();

			unsigned int getCurrentLineNumber();
			std::string getCurrentLineNumberStr(std::string Input="",unsigned int Position=0);

			static Parser* getInstance();

		};
	}
}