#pragma once
#include"Text.h"
#include"Expression.h"
#include<map>

namespace GardenScript
{
	namespace Keyword
	{
		class Parser
		{
		private:
			Parser();
			~Parser();
		protected:
			typedef std::string (*KeywordFunction)(std::string Input,unsigned int& Position);

			std::map<std::string,KeywordFunction> m_keywordsFunctions;
		private:
			Expression::Expression* parse(std::string Input,unsigned int& Position);
		public:
			static Parser* getInstance();

			bool checkIfKeyword(std::string Input);

			std::string parseKeyword(std::string Input);
		};
	}
}