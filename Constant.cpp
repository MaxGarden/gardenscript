#include"Constant.h"
#include <stdlib.h>
#include"Expression.h"

using namespace GardenScript;
using namespace Parser;

Expression::ConstantParser::ConstantParser()
{
}

Expression::ConstantParser::~ConstantParser()
{
}

Expression::ConstantParser* Expression::ConstantParser::getInstance()
{
	static ConstantParser* staticReturn=new ConstantParser();

	return staticReturn;
}

TypeParser::ReturnType Expression::ConstantParser::adjustReturnType(unsigned __int64 Number)
{
	if(Number<=UINT_MAX)
		return TypeParser::ReturnType::intType();
	else
		return TypeParser::ReturnType::int64Type();
}

Expression::ConstantOperator* Expression::ConstantParser::parseChars(std::string Input,unsigned int& Position)
{
	ConstantOperator* localResult=0;
	std::string localCharacters;
	try
	{
		if(Input[Position]=='\'')
		{
			localCharacters=GardenScript::Parser::Text::currentBody(Input,++Position,'\0','\'',1);
			
			if(localCharacters.empty())
				throw Exceptions::ExpressionException("Empty character constant!");
			else if(localCharacters.size()>1)
				throw Exceptions::ExpressionException("Too many characters in constant!");

			return new ConstantOperator((void*)&localCharacters[0],1,TypeParser::ReturnType::charType());
		}
		else if(Input[Position]=='\"')
		{
			localCharacters=GardenScript::Parser::Text::currentBody(Input,++Position,'\0','\"',1);
		
			return new ConstantOperator((void*)localCharacters.c_str(),localCharacters.size(),TypeParser::ReturnType::charType());
		}
		else
			throw Exceptions::ExpressionException("Only \"\"\" and \"\'\" is allowed to characters!");
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);

		return 0;
	}

	return localResult;
}

Expression::ConstantOperator* Expression::ConstantParser::parseNumbers(std::string Input,unsigned int& Position)
{
	try
	{
		if(Input[Position]=='0'&&Input.size()>Position+2)
		{
			if(Input[Position+1]=='x')
				return hexadecimal(Input,Position);
			else if(Input[Position+1]=='b')
				return binary(Input,Position);
			else if(Input[Position+1]=='.')
				return floating(Input,Position);
			else if(isdigit(Input[Position+1]))
				return octal(Input,Position);
			else
				return decimal(Input,Position);
				//throw Exceptions::ExpressionException("Bad suffix on number or missing \";\" before identifier!");
		}
	
		for(unsigned int i=Position;i<Input.size();++i)
		{
			if(isdigit(Input[i]))
				continue;
			else if(Input[i]=='.')
				return floating(Input,Position);
			else
				break;
		}

		return decimal(Input,Position);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);

		return 0;
	}
}

Expression::ConstantOperator* Expression::ConstantParser::decimal(std::string Input,unsigned int& Position)
{
	std::string localDecimal="";
	unsigned long long int* localValue;

	while(Position<Input.size())
	{
		if(isdigit(Input[Position]))
			localDecimal+=Input[Position];
		else
			break;

		++Position;
	}

	localValue=new unsigned __int64;

	//stroull
	*localValue=_strtoui64(localDecimal.c_str(),0,10);

	return new ConstantOperator(localValue,sizeof(unsigned __int64),adjustReturnType(*localValue));
}

Expression::ConstantOperator* Expression::ConstantParser::octal(std::string Input,unsigned int& Position)
{
	std::string localOctal="";
	unsigned long long int* localValue;

	++Position;//0

	while(Position<Input.size())
	{
		if(isdigit(Input[Position]))
		{
			if(Input[Position]>='0'&&Input[Position]<='8')
				localOctal+=Input[Position];
			else
				throw Exceptions::ExpressionException(Text::stringFormat("Illegal digit \"%c\" for base \"8\"!",Input[Position]));
		}
		else
			break;

		++Position;
	}

	localValue=new unsigned long long int;

	//stroull
	*localValue=_strtoui64(localOctal.c_str(),0,8);

	return new ConstantOperator(localValue,sizeof(unsigned long long int),adjustReturnType(*localValue));
}

Expression::ConstantOperator* Expression::ConstantParser::hexadecimal(std::string Input,unsigned int& Position)
{
	std::string localHex="";
	unsigned long long int* localValue;

	Position+=2; //0x

	while(Position<Input.size())
	{
		if(isdigit(Input[Position])||(Input[Position]>='a'&&Input[Position]<='f')||(Input[Position]>='A'&&Input[Position]<='F'))
			localHex+=Input[Position];
		else
			break;

		++Position;
	}

	localValue=new unsigned long long int;

	//stroull
	*localValue=_strtoui64(localHex.c_str(),0,16);

	return new ConstantOperator(localValue,sizeof(unsigned long long int),adjustReturnType(*localValue));
}

Expression::ConstantOperator* Expression::ConstantParser::binary(std::string Input,unsigned int& Position)
{
	std::string localBinary="";
	unsigned long long int* localValue;

	Position+=2; //0x

	while(Position<Input.size())
	{
		if(isdigit(Input[Position]))
		{
			if(Input[Position]>='0'&&Input[Position]<='1')
				localBinary+=Input[Position];
			else
				throw Exceptions::ExpressionException(Text::stringFormat("Illegal digit \"%c\" for base \"2\"!",Input[Position]));
		}
		else
			break;

		++Position;
	}

	localValue=new unsigned long long int;

	//stroull
	*localValue=_strtoui64(localBinary.c_str(),0,2);
	
	return new ConstantOperator(localValue,sizeof(unsigned long long int),adjustReturnType(*localValue));
}

Expression::ConstantOperator* Expression::ConstantParser::floating(std::string Input,unsigned int& Position)
{
	std::string localFloat="";
	long double* localValue;
	bool localComma=false;

	while(Position<Input.size())
	{
		if(isdigit(Input[Position]))
			localFloat+=Input[Position];
		else if(Input[Position]=='.'&&!localComma)
		{
			localComma=true;
			localFloat+=Input[Position];
		}
		else 
		{
			if(Input[Position]=='f')
				++Position;
			break;
		}

		++Position;
	}
		
	localValue=new long double;
	*localValue=atof(localFloat.c_str());

	return new ConstantOperator(localValue,sizeof(long double),TypeParser::ReturnType::doubleType());
}