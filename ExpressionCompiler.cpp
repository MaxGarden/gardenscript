#include"ExpressionCompiler.h"
#include"Compiler.h"
#include"Memory.h"

using namespace GardenScript;

#define _Compiler GardenScript::Compiler::Compiler::getInstance()

Expression::Compiler::Compiler()
{
}

Expression::Compiler::~Compiler()
{
}

Expression::Compiler* Expression::Compiler::getInstance()
{
	static Compiler* staticReturn=new Compiler();

	return staticReturn;
}

Expression::bytecode Expression::Compiler::compileExpression(Expression* expression)
{
	if(!expression||GardenScript::Parser::Parser::getInstance()->isError())
		return "";

	bytecode localResult;
	m_identyfiers.clear();
	m_dereferencedIdentifiers.clear();
	//memset(m_updateRegisters,0,sizeof(m_updateRegisters));
	//przy funkcji w wyra�eniu trzeba b�dzie buforowa� aktualne adresy


	m_updateIndentyfiers.clear();
	m_updateIdentifiersRegisters.clear();

	m_expressionType=expression->m_type;
	localResult=compile(expression);

	for(unsigned char i=0;i<m_updateIndentyfiers.size();++i)
	{
		if(m_dereferencedIdentifiers.find(m_updateIdentifiersRegisters[i])!=m_dereferencedIdentifiers.end())
			throw Exceptions::CompilerException("m_dereferencedIdentifiers.find(m_updateIdentifiersRegisters[i])!=m_dereferencedIdentifiers.end()");
		
		localResult.push_back(Assembler::SSC);
		localResult.append(_Compiler->adjustConstant(m_updateIndentyfiers[i]->m_stackAddress));
		localResult.push_back(m_updateIdentifiersRegisters[i]);
		localResult.push_back(m_updateIndentyfiers[i]->getSize());
		
	}

	/*for(unsigned int i=0;i<m_dereferencedIdentifiers.size();++i)
	{
		localResult.push_back(Assembler::SMR);

	}*/

	return localResult;
}

Expression::bytecode Expression::Compiler::compile(Expression* expression)
{
	m_lastRegister=0;

	if(expression->m_type==Expression::CONSTANT)
		return constantExpression((ConstantOperator*)expression);
	else if(expression->m_type==Expression::BINARY)
		return binaryExpression((BinaryOperator*)expression);
	else if(expression->m_type==Expression::IDENTIFIER)
		return identyfierExpression((IdentifierOperator*)expression);
	else if(expression->m_type==Expression::DEREFERENCE)
		return dereferenceExpression((DereferenceOperator*)expression);

	throw Exceptions::ExpressionCompilerException("Undefined expression type!");
}

Expression::bytecode Expression::Compiler::_dereferenceExpression(DereferenceOperator* expression)
{
	bytecode localResult;
	unsigned char localPointerRegister;
	unsigned char localNewPointerRegister;

	localPointerRegister=m_lastRegister;

	if(expression->getDereferenceExpression()->m_type==Expression::DEREFERENCE)
		localResult+=dereferenceExpression((DereferenceOperator*)expression->getDereferenceExpression());

	if(expression->getOperator())
	{
		localNewPointerRegister=_Compiler->nextRegister();
		
		localResult.push_back(Assembler::SRV);
		localResult.push_back(localNewPointerRegister);
		localResult.append(_Compiler->adjustConstant(expression->getObject()->m_type.getSizeByDereference()));

		localResult+=compile(expression->getDereferenceExpression());

		localResult.push_back(Assembler::ARI);
		localResult.push_back(BinaryOperator::s_multiplicationAssignOperator);
		localResult.push_back(localNewPointerRegister);
		localResult.push_back(m_lastRegister);

		if(expression->getOperator()==BinaryOperator::s_subtractionOperator)
		{
			localResult.push_back(Assembler::NEG);
			localResult.push_back(localNewPointerRegister);
		}

		localResult.push_back(Assembler::ADD);
		localResult.push_back(localNewPointerRegister);
		localResult.push_back(localPointerRegister);

		m_lastRegister=localNewPointerRegister;
	}

	return localResult;
}

Expression::bytecode Expression::Compiler::dereferenceExpression(DereferenceOperator* expression)
{
	bytecode localResult;
	unsigned char localPointerRegister;
	unsigned char localDereferencedPointerRegister;

	if(expression->getObject()->m_type.m_type.m_size>255)
		throw Exceptions::ExpressionCompilerException("Size of type is more than 255B!");

	expression->m_type=Expression::IDENTIFIER;
	localResult+=identyfierExpression(expression);
	expression->m_type=Expression::DEREFERENCE;

	localResult+=_dereferenceExpression(expression);
	localPointerRegister=m_lastRegister;
		

	/*if(m_dereferencedIdentifiers.find(localPointerRegister)==m_dereferencedIdentifiers.end())
		m_dereferencedIdentifiers.insert(std::pair<unsigned char,unsigned char>(localPointerRegister,_Compiler->nextRegister()));*/

	if(m_expressionType!=Expression::DEREFERENCE)
		localDereferencedPointerRegister=_Compiler->nextRegister();
	else
		localDereferencedPointerRegister=Assembler::RESULT;

	m_dereferencedIdentifiers.insert(std::pair<unsigned char,unsigned char>(localDereferencedPointerRegister,localPointerRegister));

	localResult.push_back(Assembler::GTM);
	localResult.push_back(localDereferencedPointerRegister);
	localResult.push_back(localPointerRegister);
	localResult.push_back((unsigned char)expression->getObject()->m_type.getSizeByDereference());

	m_lastRegister=localDereferencedPointerRegister;

	return localResult;
}

Expression::bytecode Expression::Compiler::constantExpression(ConstantOperator* expression)
{
	bytecode localResult;

	if(m_expressionType!=Expression::CONSTANT&&(*(__int64*)expression->m_value)!=ULLONG_MAX&&_Compiler->findRegister(*(__int64*)expression->m_value))
	{
		m_lastRegister=_Compiler->findRegister(*(__int64*)expression->m_value);
		return localResult;
	}

	localResult.push_back(Assembler::SRV);

	if(m_expressionType!=Expression::CONSTANT)
		localResult.push_back(_Compiler->freeRegister(*(__int64*)expression->m_value));
	else
		localResult.push_back(Assembler::RESULT);

	m_lastRegister=localResult.back();

	localResult.append(_Compiler->adjustConstant(*(__int64*)expression->m_value));

	_Compiler->setRegisterValue((unsigned char)localResult[1],*(__int64*)expression->m_value);

	return localResult;
}

Expression::bytecode Expression::Compiler::binaryExpression(BinaryOperator* expression)
{
	if(Parser::getInstance()->getOperatorAssociativity(expression->getOperator())&Parser::RIGHT_TO_LEFT)
		return _pointerExpression(expression);

	return _binaryExpression(expression);
}

Expression::bytecode Expression::Compiler::_pointerExpression(BinaryOperator* expression)
{
	bytecode localResult;
	Bytecode::Identifier* localIdenyfier;

	if(expression->m_left->m_type==Expression::CONSTANT)
		return _binaryExpression(expression);

	if(expression->m_left->m_type!=Expression::IDENTIFIER&&expression->m_left->m_type!=Expression::DEREFERENCE)
		throw Exceptions::ExpressionCompilerException("expression->m_left->m_type!=Expression::(IDENTIFIER|DEREFERENCE)");

	localIdenyfier=((IdentifierOperator*)expression->m_left)->getObject()->m_identyfier;

	if(!localIdenyfier)
		throw Exceptions::ExpressionCompilerException("Identifier is not declared!");

	if(Parser::getInstance()->getOperatorString(expression->getOperator())=="&")
	{
		if(expression->m_left->m_type==Expression::IDENTIFIER)
		{
			if(!((IdentifierOperator*)expression->m_left)->getExpression())
			{
				localResult.push_back(Assembler::MOV);
				localResult.push_back(Assembler::LAST_SCOPE_END);
				localResult.push_back(Assembler::RESULT);

				localResult.push_back(Assembler::SUC);
				localResult.push_back(Assembler::RESULT);
				localResult.append(_Compiler->adjustConstant(localIdenyfier->m_stackAddress));

				return localResult;
			}
			else
			{
				if(((IdentifierOperator*)expression->m_left)->getExpression()->m_type!=Expression::DEREFERENCE)
					throw Exceptions::ExpressionCompilerException("Cannot get an address of non identyfier/dereference!");

				localResult+=dereferenceExpression((DereferenceOperator*)((IdentifierOperator*)expression->m_left)->getExpression());
			}
		}
		else if(expression->m_left->m_type==Expression::DEREFERENCE)
			localResult+=dereferenceExpression((DereferenceOperator*)expression->m_left);
		else
			throw Exceptions::ExpressionCompilerException("Cannot get an address of non identyfier/dereference!");

		localResult.push_back(Assembler::MOV);
		localResult.push_back(m_dereferencedIdentifiers.find(m_lastRegister)->second);
		localResult.push_back(Assembler::RESULT);

		m_lastRegister=Assembler::RESULT;
		
		return localResult;
	}


	return _binaryExpression(expression);
}

Expression::bytecode Expression::Compiler::_binaryExpression(BinaryOperator* expression)
{
	bytecode localResult;
	unsigned char localLeftRegister=0;
	unsigned char localRightRegister=0;

	unsigned char localOpcode=Assembler::ARI;
	bool localLeftFloat,localRightFloat;

	if(expression->m_right)
	{
		localResult.append(compile(expression->m_right));

		if(expression->m_right->m_type==Expression::CONSTANT)
			localRightRegister=_Compiler->findRegister(*(__int64*)(((ConstantOperator*)expression->m_right)->m_value));
		else
			localRightRegister=m_lastRegister;
	

		if(localRightRegister==Assembler::RESULT)
		{
			localResult.push_back(Assembler::MOV);
			localResult.push_back(localRightRegister);
			localResult.push_back((char)Assembler::BUFFER);

			localRightRegister=Assembler::BUFFER;
		}
	}
	
	localResult.append(compile(expression->m_left));

	//je�eli binary nie ma prawej strony (np ++) to lewa strona NIE MO�E by� const


	if(expression->m_left->m_type==Expression::CONSTANT)
		localLeftRegister=_Compiler->findRegister(*(__int64*)(((ConstantOperator*)expression->m_left)->m_value));
	else
		localLeftRegister=m_lastRegister;

	localLeftFloat=GardenScript::Expression::Parser::getInstance()->isFloat(expression->m_left->calculateReturnType());
	
	if(expression->m_right)
		localRightFloat=GardenScript::Expression::Parser::getInstance()->isFloat(expression->m_right->calculateReturnType());
	else 
		localRightFloat=false;

	if(localLeftFloat)
	{
		localOpcode=Assembler::ARF;

		if(!localRightFloat)
		{
			localResult.push_back(Assembler::ITD);
			localResult.push_back(localRightRegister);
		}
	}
	else if(localRightFloat)
	{
		localOpcode=Assembler::ARI;
		localResult.push_back(Assembler::DTI);
		localResult.push_back(localRightRegister);
	}
	
	localResult.push_back(localOpcode);
	localResult.push_back(expression->getOperator());
	localResult.push_back(localLeftRegister);
	localResult.push_back(localRightRegister);

	if(Parser::getInstance()->isModifyingOperator(expression->getOperator())/*&&expression->m_left->m_type==Expression::IDENTIFIER*/)
	{

		if(expression->m_left->m_type!=Expression::IDENTIFIER&&expression->m_left->m_type!=Expression::DEREFERENCE)
			throw Exceptions::CompilerException("expression->m_left->m_type!=Expression::IDENTIFIER&&expression->m_type!=Expression::DEREFERENCE");

		
		if(expression->m_left->m_type==Expression::IDENTIFIER&&((IdentifierOperator*)expression->m_left)->getExpression()&&((IdentifierOperator*)expression->m_left)->getExpression()->m_type==Expression::DEREFERENCE)
		{
			localResult.push_back(Assembler::SMR);
			localResult.push_back(m_dereferencedIdentifiers.find(localLeftRegister)->second);
			localResult.push_back(m_dereferencedIdentifiers.find(localLeftRegister)->first);
			localResult.push_back(((IdentifierOperator*)expression->m_left)->getObject()->m_type.getSizeByDereference());
		}
		else
		{
			m_updateIndentyfiers.push_back(((IdentifierOperator*)expression->m_left)->getObject()->m_identyfier);
			m_updateIdentifiersRegisters.push_back(localLeftRegister);
		}

		/*localResult.push_back(Assembler::MOV);
		localResult.push_back(Assembler::RESULT);
		localResult.push_back(*/
	}

	m_lastRegister=Assembler::RESULT;

	return localResult;
}

Expression::bytecode Expression::Compiler::typecast(IdentifierOperator* expression)
{
	bytecode localResult;
	bool localExpressionFloat;
	bool localTypeFloat;
	int localDifference;

	localTypeFloat=GardenScript::Expression::Parser::getInstance()->isFloat(expression->getObject()->m_type);
	localExpressionFloat=GardenScript::Expression::Parser::getInstance()->isFloat(expression->getExpression()->calculateReturnType());

	localDifference=GardenScript::Compiler::Compiler::getInstance()->getMachineType()-(int)expression->getObject()->m_type.getSize();
	localDifference*=8;
	if(localTypeFloat!=localExpressionFloat)
	{
		if(localExpressionFloat)
			localResult.push_back(Assembler::DTI);
		else
			localResult.push_back(Assembler::ITD);

		localResult.push_back(m_lastRegister);
	}

	if(!localTypeFloat&&localDifference>0)
	{
		if(!(expression->getObject()->m_type.m_flags&TypeParser::F_UNSIGNED))
		{
			localResult.push_back(Assembler::CRS);
			localResult.push_back(m_lastRegister);
			localResult.push_back((unsigned char)localDifference);
		}
		else
		{
			localResult.push_back(Assembler::CRU);
			localResult.push_back(m_lastRegister);
			localResult.push_back((unsigned char)expression->getObject()->m_type.getSize()*8);
		}

	}
				
	return localResult;
}

Expression::bytecode Expression::Compiler::identyfierExpression(IdentifierOperator* expression)
{
	bytecode localResult;
	Bytecode::Identifier* localIdenyfier=expression->getObject()->m_identyfier;
	unsigned int localCurrentScopeIndex=Memory::MemoryManager::getInstance()->getCurrentScopeIndex();
	unsigned char localRegister=0;
	bool localChangedExpressionType=false;
	int localDifference;

	if(!localIdenyfier)
		throw Exceptions::ExpressionCompilerException("Identifier is not declared!");

	//mo�na zrobi� globalne m_identyfiers, ale teraz mi si� nie chc� ;>
	//if(expression->m_type!=Expression::IDENTIFIER)
	//	throw Exceptions::ExpressionCompilerException("co� si� pojeba�o");
	
	if(expression->getExpression())
	{
		if(m_expressionType==Expression::IDENTIFIER)
		{
			m_expressionType=expression->getExpression()->m_type;
			localChangedExpressionType=true;
		}

		localResult+=compile(expression->getExpression());
		
		if(m_lastRegister==Assembler::RESULT&&!localChangedExpressionType)
		{
			localRegister=_Compiler->nextRegister();
		
			localResult.push_back(Assembler::MOV);
			localResult.push_back(m_lastRegister);
			localResult.push_back(localRegister);

			m_lastRegister=localRegister;
		}
		else if(localChangedExpressionType&&m_lastRegister!=Assembler::RESULT)
			throw Exceptions::ExpressionCompilerException("Co� si� jeb�o");

		if(localIdenyfier->m_size==0) 
			localResult+=typecast(expression);
	}
	else
	{
		if(m_identyfiers.find(localIdenyfier)==m_identyfiers.end())
		{
			if(m_expressionType!=Expression::IDENTIFIER)
			{
				localRegister=255;

				while(localRegister==255)
					localRegister=_Compiler->nextRegister();

			}
			else
				localRegister=Assembler::RESULT;

			m_identyfiers.insert(std::pair<Bytecode::Identifier*,unsigned char>(localIdenyfier,localRegister));
		
			if(!(expression->getObject()->m_type.m_flags&TypeParser::F_UNSIGNED))
			{
				localDifference=GardenScript::Compiler::Compiler::getInstance()->getMachineType()-(int)expression->getObject()->m_type.getSize();
				localDifference*=8;
	
				if(localDifference<0)
					throw Exceptions::ExpressionCompilerException("localDifference<0");

				localResult.push_back(Assembler::GCS);
			}
			else
			{
				localResult.push_back(Assembler::GCU);
				localDifference=expression->getObject()->m_type.m_type.m_size;
			}

			localResult.push_back(localRegister);
			localResult.append(_Compiler->adjustConstant(localIdenyfier->m_stackAddress));
			localResult.push_back(localDifference);
		}

		m_lastRegister=m_identyfiers.find(localIdenyfier)->second;
	}

	
	
	return localResult;
}