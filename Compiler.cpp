#include"Compiler.h"
#include"Exceptions.h"
#include"VirtualMemory.h"
#include"DeclarationCompiler.h"
#include"ExpressionCompiler.h"

using namespace GardenScript;

Compiler::Compiler::Compiler()
{
	m_machineType=_64b;
	m_isSetType=false;
	m_registersIndex=Assembler::FIRST_FREE_REGISTER;
}

Compiler::Compiler::~Compiler()
{
}

Compiler::Compiler* Compiler::Compiler::getInstance()
{
	static Compiler* staticReturn=new Compiler();

	return staticReturn;
}

std::string Compiler::Compiler::adjustConstant(__int64 Constant)
{
	std::string localResult;

	if(m_machineType==_32b)
	{
		int localBuffer=(int)Constant;
		localResult.resize(sizeof(int));

		memcpy(&localResult[0],&localBuffer,sizeof(localBuffer));
	}
	else
	{
		localResult.resize(sizeof(__int64));
		memcpy(&localResult[0],&Constant,sizeof(__int64));
	}

	return localResult;
}

std::string Compiler::Compiler::adjustConstant(__int64 Constant,unsigned char Size)
{
	std::string localResult;

	localResult.resize(Size);
	memcpy(&localResult[0],&Constant,Size);
	
	return localResult;
}

std::string Compiler::Compiler::adjustJPDConstant(__int64 Constant)
{
	std::string localResult;

	Constant+=m_machineType+1;

	if(m_machineType==_32b)
	{
		int localBuffer=(int)Constant;
		localResult.resize(sizeof(int));

		memcpy(&localResult[0],&localBuffer,sizeof(localBuffer));
	}
	else
	{
		localResult.resize(sizeof(__int64));
		memcpy(&localResult[0],&Constant,sizeof(__int64));
	}

	return localResult;
}


unsigned char Compiler::Compiler::findRegister(__int64 Constant)
{
	if(Constant!=ULLONG_MAX)
	{
		for(unsigned int i=1;i<Memory::VirtualMemory::REGISTER_SIZE;++i)
		{
			if(Memory::VirtualMemory::getInstance()->m_registers[i]==Constant)
				return i;
		}
	}
		
	return 0;
}

unsigned char Compiler::Compiler::freeRegister(__int64 Constant)
{
	if(Constant!=ULLONG_MAX)
	{
		for(unsigned int i=m_registersIndex;i<Memory::VirtualMemory::REGISTER_SIZE;++i)
		{
			if(Memory::VirtualMemory::getInstance()->m_registers[i]==(int)Constant)
				return i;
		}
	}
		
	if(m_registersIndex>=255)
		m_registersIndex=Assembler::FIRST_FREE_REGISTER;

	return (unsigned char)++m_registersIndex;
}

unsigned char Compiler::Compiler::nextRegister()
{
	if(m_registersIndex>=255)
		m_registersIndex=Assembler::FIRST_FREE_REGISTER;

	Memory::VirtualMemory::getInstance()->m_registers[++m_registersIndex]=ULLONG_MAX;

	return (unsigned char)m_registersIndex;
}

void Compiler::Compiler::setRegisterValue(unsigned char Register,__int64 Value)
{
	Memory::VirtualMemory::getInstance()->m_registers[Register]=Value;
}

Compiler::Compiler::TARGET_TYPE Compiler::Compiler::getMachineType()
{
	return m_machineType;
}

Compiler::bytecode Compiler::Compiler::compile(std::string Input,unsigned int& Position)
{
	bytecode localResult;

	Declaration::Compiler::getInstance()->pushScope(localResult);
	localResult.append(Parser::Parser::getInstance()->parse(Input,Position,localResult.size()));
	Declaration::Compiler::getInstance()->popScope(localResult);

	return localResult;
}