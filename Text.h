#pragma once
#include"Parser.h"

namespace GardenScript
{
	namespace Parser
	{
		namespace Text
		{
			const std::string constStopChars=";\0 "; //' '- white chars
			const std::string constAllowedChars="@#"; //@-alpha #-digit

			const std::string constAllowedNameChars="_";
			const std::string constSignleArgumentsOperators="!&*";
			const std::string constDoubleArgumentsOperators="%^+-*/=";
			const std::string constAssigmentOperators="+/*-=^%!";

			const std::string constBeginBrackets="({[<";
			const std::string constEndBrackets=")}]>";

			const char constEndExpressionChar=';';

			void adjust(std::string Input,unsigned int& Postion);
			unsigned char currentChar(std::string Input,unsigned int& Postion,bool Update=true);
			
			unsigned char _currentChar(std::string Input,unsigned int& Position,bool Update=true); //currentChar bez adjust
			unsigned char __currentChar(std::string Input,unsigned int& Position,bool Update=true); //currentChar bez adjust i obs�ugi \\

			//std::string currentString(std::string Input,unsigned int& Position,std::string AllowedChars=constAllowedChars,std::string StopChars=constStopChars,bool Update=true);
		
			std::string currentString(std::string Input,unsigned int& Position,bool Update=true);
			std::string currentLine(std::string Input,unsigned int& Position,unsigned int& Lines,bool Update=true);
			std::string currentLine(std::string Input,unsigned int& Position,bool Update=true,bool RAW=false);
			std::string currentName(std::string Input,unsigned int& Position,bool Update=true);
			bool isName(std::string Input);
			
			std::string currentExpression(std::string Input,unsigned int& Position,unsigned char StopChar,bool Update=true); //zatrzymuje si na danym znaku nie licz�c wyst�pie� we wszyskich nawiasach

			unsigned int patternIndexChar(char Char,std::string Pattern);

			bool checkChar(char Char,std::string Pattern);
			bool checkString(std::string  String,std::vector<std::string> Pattern);

			bool isAllowedNameChar(unsigned char Char);

			std::string currentOperator(std::string Input,unsigned int& Position,long Flags=0,char* OperatorID=0);

			bool equalString(std::string Input,unsigned int& Position,std::string Pattern,bool WhiteChars=false,bool Update=true);
			std::string cutString(std::string Input,unsigned int Begin,int End=-1);
			std::string stringFormat(std::string String,...);

			std::string currentArgument(std::string Input,unsigned int& Position,std::string EndChars,bool Update=true);

			std::string currentBody(std::string Input,unsigned int& Position,char Incrementation,char Decrementation,unsigned int Counter=0,bool Update=true);
		}
	}
}