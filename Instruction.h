#pragma once
#include"Text.h"
#include<map>

namespace GardenScript
{
	namespace Instruction
	{
		class Parser
		{
		typedef  std::string (*InstructionFunction)(std::string Input,unsigned int& Position);
		private:
			Parser();
			~Parser();
		protected:
			std::map<std::string,InstructionFunction> m_keywords;
		private:
			std::string parse(std::string Input,unsigned int& Position);

			static std::string parseIf(std::string Input,unsigned int& Position);
			static std::string parseElse(std::string Input,unsigned int& Position,bool Self=false);
			static std::string parseWhile(std::string Input,unsigned int& Position);
			static std::string parseDoWhile(std::string Input,unsigned int& Position);
			static std::string parseFor(std::string Input,unsigned int& Position);
			static std::string parseBreak(std::string Input,unsigned int& Position);
			static std::string parseContinue(std::string Input,unsigned int& Position);
			static std::string parsePrint(std::string Input,unsigned int& Position);
			static std::string parseReturn(std::string Input,unsigned int& Position);
		public:
			std::string parseInstruction(std::string Input,unsigned int& Position);
			
			bool checkIfInstruction(std::string Input,unsigned int Position);

			static Parser* getInstance();
		};
	}
};