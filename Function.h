#pragma once
#include"Memory.h"
#include"Type.h"

namespace GardenScript
{
	namespace Memory
	{
		class Function:public MemoryObject
		{
		public:
			struct Variant
			{
				Variant();
				~Variant();

				Scope* m_scope;
				std::vector<TypeParser::ReturnType*> m_arguments;
				std::string m_body;
			};
		protected:
			std::vector<Variant*> m_variants;
			TypeParser::ReturnType* m_returnType;
		public:
			Function();
			~Function();

			void addArgument(unsigned int Variant,TypeParser::ReturnType* Argument);
			Scope* getScope(unsigned int Variant);

			void setBody(unsigned int Variant,std::string Body);
			std::string getBody(unsigned int Variant);

			void setScope(unsigned int Variant,Scope* scope);
			unsigned int getIndexVariant(std::vector<TypeParser::ReturnType*> Arguments,Scope* scope);//m_variants.size()+1 je�li nie ma

			unsigned int getVariantsSize();
			void addVariant();
		};
	}
}