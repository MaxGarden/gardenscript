#include"Identifier.h"
#include"Memory.h"

using namespace GardenScript::Expression;

IdentifierParser::IdentifierParser()
{
}

IdentifierParser::~IdentifierParser()
{
}

IdentifierParser* IdentifierParser::getInstance()
{
	static IdentifierParser* staticReturn=new IdentifierParser();

	return staticReturn;
}

IdentifierOperator* IdentifierParser::parse(std::string Input,unsigned int& Position)
{
	IdentifierOperator* localResult=0;
	Memory::MemoryObject* localObject=0;

	try
	{
		localObject=GardenScript::Memory::MemoryManager::getInstance()->getObject(GardenScript::Parser::Text::currentName(Input,Position));
		
		localResult=new IdentifierOperator(localObject);
	}
	catch(Exceptions::Exception& e)
	{
		GardenScript::Parser::Parser::getInstance()->addError(e);

		return 0;
	}

	return localResult;
}