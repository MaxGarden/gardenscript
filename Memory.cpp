#include"Memory.h"
#include"Exceptions.h"
#include"Text.h"

using namespace GardenScript::Memory;

MemoryObject::MemoryObject()
{
	m_multipleDefine=false;
	m_defaultValue=0;
}

Scope::Scope()
{
	m_currentAddress=0;
}

Scope::~Scope()
{
	for(std::map<std::string,MemoryObject*>::iterator it=m_scope.begin();it!=m_scope.end();++it)
		delete (*it).second;
}

void Scope::addObject(MemoryObject* Object)
{
	if(m_scope.find(Object->m_type.m_name)==m_scope.end())
		m_scope.insert(std::pair<std::string,MemoryObject*>(Object->m_type.m_name,Object));
	else
		throw GardenScript::Exceptions::MemoryException(GardenScript::Parser::Text::stringFormat("Object \"%s\" is already defined in this scope!",Object->m_type.m_name.c_str()));
}

bool Scope::isObject(std::string Name)
{
	return m_scope.find(Name)!=m_scope.end();
}

MemoryObject* Scope::getObject(std::string Name)
{
	std::map<std::string,MemoryObject*>::iterator localIterator=m_scope.find(Name);

	if(localIterator==m_scope.end())
		throw GardenScript::Exceptions::MemoryException(GardenScript::Parser::Text::stringFormat("Undeclared identifier \"%s\" in this scope!",Name.c_str()));
	else
		return localIterator->second;
}

void Scope::setObject(std::string Name,MemoryObject* Object)
{
	std::map<std::string,MemoryObject*>::iterator localIterator=m_scope.find(Name);

	if(localIterator==m_scope.end())
		throw GardenScript::Exceptions::MemoryException(GardenScript::Parser::Text::stringFormat("Undeclared identifier \"%s\" in this scope!",Name.c_str()));
	else
		localIterator->second=Object;
}

unsigned __int64 Scope::getNextAddress(unsigned __int64 Size,bool Reserve)
{
	m_currentAddress+=Size;

	return m_currentAddress-Size;
}

__int64 Scope::getScopeSize()
{
	return m_currentAddress;
}

MemoryManager::MemoryManager()
{
	m_memory.push_back(new Scope); //zakres globalny
}

MemoryManager::~MemoryManager()
{
	for(unsigned int i=0;i<m_memory.size();++i)
			delete m_memory[i];
}

MemoryManager* MemoryManager::getInstance()
{
	static MemoryManager* staticReturn=new MemoryManager();

	return staticReturn;
}

unsigned int MemoryManager::getCurrentScopeIndex()
{
	if(m_memory.empty())
		throw Exceptions::MemoryException("Scope doesn't exist!");

	return m_memory.size()-1;
}

Scope* MemoryManager::getCurrentScope()
{
	return m_memory.empty()?0:m_memory.back();
}

bool MemoryManager::isObject(std::string Name)
{
	for(unsigned int i=0;i<m_memory.size();++i)
		if(m_memory[i]->isObject(Name))
			return true;
		
	return false;
}

MemoryObject* MemoryManager::getObject(std::string Name)
{
	for(unsigned int i=0;i<m_memory.size();++i)
	{
		if(m_memory[i]->isObject(Name))
			return m_memory[i]->getObject(Name);
	}

	throw GardenScript::Exceptions::MemoryException(GardenScript::Parser::Text::stringFormat("Undeclared identifier \"%s\"!",Name.c_str()));
	return 0;
}

void MemoryManager::pushScope(Scope* scope)
{
	m_memory.push_back(scope);
}

void MemoryManager::popScope()
{
	if(m_memory.size()<=1)
		throw Exceptions::MemoryException("popscope with m_memory.size()<=1");

	m_memory.pop_back();
}