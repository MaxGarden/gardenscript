#pragma once
#include"Text.h"
#include"Parser.h"
#include"Bytecode.h"
#include"Type.h"

namespace GardenScript
{
	namespace Expression
	{
		class ConstantOperator;

		class ConstantParser
		{
		private:
			ConstantParser();
			~ConstantParser();
		private:
			TypeParser::ReturnType adjustReturnType(unsigned __int64 Number);
			ConstantOperator* hexadecimal(std::string Input,unsigned int& Position);
			ConstantOperator* decimal(std::string Input,unsigned int& Position);
			ConstantOperator* octal(std::string Input,unsigned int& Position);
			ConstantOperator* binary(std::string Input,unsigned int& Position);
			ConstantOperator* floating(std::string Input,unsigned int& Position);
		public:
			static ConstantParser* getInstance();

			ConstantOperator* parseNumbers(std::string Input,unsigned int& Position); 
			ConstantOperator* parseChars(std::string Input,unsigned int& Position);
		};
	}
}