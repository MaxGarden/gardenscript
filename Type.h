#pragma once
#include<map>
#include<vector>

namespace GardenScript
{
	namespace TypeParser
	{
		enum FLAGS
		{
			F_AUTO		=0x000000F,
			F_POD		=0xFFFFF00,
			F_FLOATPOD	=0xFF00F00,
			F_STATIC	=0xF000000, 
			F_CONST		=0x0F00000,
			F_SIGNED	=0x00F0000,
			F_UNSIGNED	=0x000F000,
			F_LOCAL		=0x0000F00,
			F_DEFAULT	=F_STATIC|F_CONST,
			
			A_AUTO		=F_STATIC|F_CONST|F_LOCAL,

			E_SIGNED	=F_UNSIGNED|F_SIGNED,

			T_FLOAT		=0x00000F00,
			T_POINTER	=0x000000F0,
			T_REFERENCE	=0x0000000F
		};


		struct Type
		{
			Type();
			Type(std::string Name,unsigned int Size,bool POD,long AllowedFlags,bool Float);
			~Type();

			unsigned int m_size;
			bool m_pod;
			long m_allowedFlags;
			bool m_float;
			std::string m_name;//nazwa typu, nie rt!!!
			//co� do trzymania operator�w;

			Type* copy();

			bool operator==(const Type& type);
		};

		struct ReturnType
		{
			ReturnType();
			~ReturnType();

			std::string m_name;
			Type m_type;
			long m_flags;
			unsigned int m_pointer;
			std::vector<unsigned int> m_arrayDimension; //UNIT_MAX je�li nie okre�lony

			ReturnType* copy();

			std::string getReturnTypeName();

			unsigned int getSize();
			unsigned __int64 getStackSize();
			unsigned int getSizeByDereference();

			static const ReturnType charType();
			static const ReturnType intType();
			static const ReturnType int64Type();
			static const ReturnType pointerType();
			static const ReturnType doubleType();

			static bool compare(ReturnType* cmp1,ReturnType* cmp2);
		};

		class Parser
		{
		private:
			Parser();
			~Parser();
		protected:
			std::map<std::string,Type*> m_types;
			std::map<std::string,long> m_flags;
		private:
			long parseFlags(std::string Input,unsigned int& Position);
			ReturnType* parse(std::string Input,unsigned int& Position);
		public:
			static Parser* getInstance();
	
			ReturnType* parseType(std::string Input,unsigned int& Position);

			bool checkIfType(std::string Input,unsigned int Position);
		};
	}
}